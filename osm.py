# Copyright (C) 2018-2021 Teemu Ikonen <tpikonen@gmail.com>

# This file is part of Taival.
# Taival is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License version 3 as published by the
# Free Software Foundation. See the file COPYING for license text.

import itertools
import logging
import overpy
import time
from collections import defaultdict
from util import (
    get_multivalue_tag,
    ldist2,
)

log = logging.getLogger(__name__)

api = overpy.Overpass(
    retry_timeout=30,
    max_retry_count=10)


def apiquery(query):
    waittimes = [2, 3, 4, 8]  # min
    for t in waittimes:
        try:
            rr = api.query(query)
            return rr
        except overpy.exception.OverpassTooManyRequests:
            tsec = t * 60
            log.info(f"Too many Overpass requests, waiting {tsec} seconds.")
            time.sleep(tsec)
    log.error("Giving up on Overpass requests")
    raise overpy.exception.OverpassTooManyRequests("Giving up")


# Areas must be initialized by e.g. hsl.overpass_area before queries
area = None
stopref_area = None

stoptags = {
    "train": [
        {"railway": "station"},
        {"railway": "halt"},
        {"railway": "platform"},
        {"railway": "platform_edge"},
        # Use stop_area relations for multi-platform train stops in HSL data
        {"public_transport": "stop_area",
         "train": "yes"},
    ],
    "light_rail": [
        {"railway": "station",
         "station": "light_rail"},
        {"railway": "halt",
         "light_rail": "yes"},
        {"railway": "platform",
         "light_rail": "yes"},
        {"railway": "platform_edge",
         "light_rail": "yes"},
    ],
    "subway": [
        {"railway": "station",
         "station": "subway"},
        {"railway": "halt",
         "subway": "yes"},
        {"railway": "platform",
         "subway": "yes"},
        {"railway": "platform_edge",
         "subway": "yes"},
    ],
    "monorail": [
        {"railway": "station",
         "station": "monorail"},
        {"railway": "halt",
         "monorail": "yes"},
        {"railway": "platform",
         "monorail": "yes"},
        {"railway": "platform_edge",
         "monorail": "yes"},
    ],
    "tram": [
        {"railway": "tram_stop"},
    ],
    "bus": [
        {"amenity": "bus_station"},
        {"highway": "bus_stop"},
    ],
    "trolleybus": [
        {"amenity": "bus_station",
         "trolleybus": "yes"},
        {"highway": "bus_stop",
         "trolleybus": "yes"},
    ],
    "ferry": [
        {"amenity": "ferry_terminal"},
    ],
    "aerialway": [
        {"aerialway": "station"},
    ],
}

stationtags = {
    "bus": [{"amenity": "bus_station"}],
    # "station": None means [!station] in overpass (i.e. no station tag)
    "train": [{"railway": "station", "station": None}],
    "subway": [{"railway": "station", "station": "subway"}],
    "light_rail": [{"railway": "station", "station": "light_rail"}],
    "tram": [{"railway": "station", "station": "tram"}],
}

citybiketags = [{"amenity": "bicycle_rental"}]

bikeparktags = [{"amenity": "bicycle_parking"}]

xtype2osm = {
    'n': "node",
    'w': "way",
    'r': "relation",
}

# Helper functions


def relid2url(relid):
    return "https://www.openstreetmap.org/relation/" + str(relid)


def obj2url(s):
    return "https://www.openstreetmap.org/{}/{}".format(
        xtype2osm[s["x:type"]], s["x:id"])


def stoplist2links(stoplist):
    return " ".join("[{} {}]".format(obj2url(s), s["ref"]) for s in stoplist)


def member_coord(x):
    """Return (lat, lon) coordinates from a resolved relation member.

    Could be improved by calculating center of mass from ways etc.
    """
    if isinstance(x, overpy.Way):
        x.get_nodes(resolve_missing=True)
        lats = [float(v.lat) for v in x.nodes]
        lons = [float(v.lon) for v in x.nodes]
        lat = sum(lats) / len(x.nodes)
        lon = sum(lons) / len(x.nodes)
    elif isinstance(x, overpy.Relation):
        m = x.members[0].resolve(resolve_missing=True)
        (lat, lon) = member_coord(m)
    else:
        lat = float(x.lat)
        lon = float(x.lon)
    return (lat, lon)


def ndist2(n1, n2):
    """Return distance metric squared for two nodes n1 and n2."""
    return (n1.lat - n2.lat)**2 + (n1.lon - n2.lon)**2


# route relations


overpy_route_cache = {k: None for k in list(stoptags.keys()) + ["minibus"]}


def get_route_rr(mode="bus"):
    """Return a (cached) overpy.Result object with all the routes for mode in area."""
    if overpy_route_cache.get(mode, None):
        return overpy_route_cache[mode]
    q = (f'[out:json][timeout:600];{area}\n'
         f'rel(area.hel)[type=route][route="{mode}"][ref];(._;>;>;);out body;')
    log.debug(q)
    rr = apiquery(q)
    overpy_route_cache[mode] = rr
    return rr


def all_linerefs(mode, networks):
    """Return a lineref:[urllist] dict where network tag of linerefs is in networks.

    URLs points to the relations in OSM.
    """
    rr = get_route_rr(mode)
    refs = defaultdict(list)
    for r in rr.relations:
        if ('ref' in r.tags.keys()
                and any(n in networks for n in get_multivalue_tag(r.tags, 'network'))):
            refs[r.tags['ref']].append(relid2url(r.id))
    return refs


def rels_query(lineref, mode="bus"):
    """Return public transport routes in area with a given lineref.

    Makes a direct query.
    """
    q = (f'{area}\n'
         f'rel(area.hel)[route="{mode}"][ref="{lineref}"];(._;>;>;);out body;')
    log.debug(q)
    rr = apiquery(q)
    return rr.relations


def rels_refless(mode):
    """Return routes for mode which do not have a ref tag."""
    q = (f'[out:json][timeout:300];{area}\n'
         f'rel(area.hel)[type=route][route="{mode}"][!ref];(._;);out tags;')
    log.debug(q)
    rr = apiquery(q)
    return rr.relations


def rels(lineref, mode="bus", networks=[]):
    """Return all lines corresponding to lineref and mode in area."""
    rr = get_route_rr(mode)
    retval = [r for r in rr.relations if r.tags.get("ref", None) == lineref]
    if networks:
        retval = [r for r in retval
                  if any(n in networks for n in get_multivalue_tag(r.tags, 'network'))]
    return retval


def rel_members_w_roles(rel, roleprefixes):
    """Return relation members where role starts with a string from roleprefixes.

    The return value is a tuple (lat, lon, ref, name, role).
    """
    retval = []
    elems = [(mem.resolve(resolve_missing=True), mem.role)
             for mem in rel.members
             if mem.role and any(mem.role.startswith(p) for p in roleprefixes)]
    for x, role in elems:
        (lat, lon) = member_coord(x)
        ref = x.tags.get('ref', '<no ref in OSM>')
        name = x.tags.get('name', '<no name in OSM>')
        retval.append((float(lat), float(lon), ref, name, role))
    return retval


def route_platforms(rel):
    """Return members of route relations with role 'platform*'.

    The return value is a tuple (lat, lon, ref, name, role).
    """
    return rel_members_w_roles(rel, ('platform',))


def route_stops(rel):
    """Return members of route relations with role 'stop*'.

    The return value is a tuple (lat, lon, ref, name, role).
    """
    return rel_members_w_roles(rel, ('stop',))


def route_platforms_and_stops(rel):
    """Return 'platform' and 'stop' members for a route relation.

    The return value is a tuple (lat, lon, ref, name, role).
    """
    return rel_members_w_roles(rel, ('platform', 'stop'))


def route_platforms_or_stops(rel):
    """Return platforms for a route relation.

    If there are no relation members with a role 'platform*',
    return members with role 'stop*' instead.

    The return value is a tuple (lat, lon, ref, name, role).
    """
    if any(m.role.startswith('platform') for m in rel.members if m.role):
        return route_platforms(rel)
    else:
        return route_stops(rel)


def route_shape(rel):
    """Return a ([[lat,lon]], gaps) route shape from overpy relation.

    Value 'gaps' is True if the ways in route do not share endpoint nodes.
    """
    ways = [mem.resolve() for mem in rel.members
            if isinstance(mem, overpy.RelationWay) and (
                not mem.role or mem.role in ('forward', 'backward'))]
    gaps = False
    if not ways:
        return ([], gaps)
    elif len(ways) == 1:
        latlon = [[float(n.lat), float(n.lon)] for n in ways[0].nodes]
        # Determine correct orientation for a single way route
        stops = [mem.resolve() for mem in rel.members if mem.role == "stop"]
        if stops:
            spos = member_coord(stops[0])
            if ldist2(spos, latlon[0]) > ldist2(spos, latlon[-1]):
                latlon.reverse()
            return (latlon, gaps)
        plats = [mem.resolve()
                 for mem in rel.members if mem.role == "platform"]
        if plats:
            ppos = member_coord(plats[0])
            if ldist2(ppos, latlon[0]) > ldist2(ppos, latlon[-1]):
                latlon.reverse()
            return (latlon, gaps)
        # Give up and do not orient
        return (latlon, gaps)
    # Initialize nodes list with first way, correctly oriented
    gap_after_first = False
    if (ways[0].nodes[-1] == ways[1].nodes[0]
            or ways[0].nodes[-1] == ways[1].nodes[-1]):
        nodes = ways[0].nodes
    elif (ways[0].nodes[0] == ways[1].nodes[0]
            or ways[0].nodes[0] == ways[1].nodes[-1]):
        nodes = ways[0].nodes[::-1]  # reverse
    elif ways[1].nodes[0] == ways[1].nodes[-1]:
        if not ways[1].tags.get("junction", None) == "roundabout":
            log.debug(f"Circular (2nd) way is not a roundabout in relation {rel.id} !")  # noqa: E501
        if ways[0].nodes[-1] in ways[1].nodes:
            nodes = ways[0].nodes
        elif ways[0].nodes[0] in ways[1].nodes:
            nodes = ways[0].nodes[::-1]
        else:
            gap_after_first = True
    else:
        gap_after_first = True
    if gap_after_first:  # Orient first segment in case of a gap
        gaps = True
        begmin = min(ndist2(ways[0].nodes[0], ways[1].nodes[0]),
                     ndist2(ways[0].nodes[0], ways[1].nodes[-1]))
        endmin = min(ndist2(ways[0].nodes[-1], ways[1].nodes[0]),
                     ndist2(ways[0].nodes[-1], ways[1].nodes[-1]))
        if endmin < begmin:
            nodes = ways[0].nodes
        else:
            nodes = ways[0].nodes[::-1]
    # Combine nodes from the rest of the ways to a single list,
    # flip ways when needed, split roundabout ways
    # Iterate with index, because we need to peek ways[i+1] for roundabouts
    for i in range(1, len(ways)):
        w = ways[i]
        if w.nodes[0] == w.nodes[-1]:
            if not w.tags.get("junction", None) == "roundabout":
                log.debug(f"Circular way is not a roundabout in rel {rel.id}")
            startind = w.nodes.index(nodes[-1]) if nodes[-1] in w.nodes else None  # noqa: E501
            wnext = ways[i + 1] if (i + 1) < len(ways) else None
            if wnext:
                # This does not work with 2 roundabouts in a row
                if wnext.nodes[0] in w.nodes:
                    nextstart = wnext.nodes[0]
                elif wnext.nodes[-1] in w.nodes:
                    nextstart = wnext.nodes[-1]
                else:
                    nextstart = None
                nextind = w.nodes.index(nextstart) if nextstart else None
                # We don't add a duplicate startind node, but do add the
                # nextind node to the end of the node list in assignments
                # below, hence the +1's in slices
                if (startind is not None) and (nextind is not None):
                    if startind < nextind:
                        nodes.extend(w.nodes[(startind + 1):(nextind + 1)])
                    else:
                        nodes.extend(w.nodes[(startind + 1):])
                        nodes.extend(w.nodes[:(nextind + 1)])
                elif startind is not None:
                    gaps = True
                    nodes.extend(w.nodes[(startind + 1):])
                    # include startind again
                    nodes.extend(w.nodes[:(startind + 1)])
                elif nextind is not None:
                    gaps = True
                    nodes.extend(w.nodes[(nextind + 1):])
                    nodes.extend(w.nodes[:(nextind + 1)])
            else:  # w is last way
                if startind is not None:
                    nodes.extend(w.nodes[(startind + 1):])
                    nodes.extend(w.nodes[:(startind + 1)])
                else:
                    gaps = True
                    nodes.extend(w.nodes)
        elif nodes[-1] == w.nodes[0]:
            nodes.extend(w.nodes[1:])
        elif nodes[-1] == w.nodes[-1]:
            nodes.extend(w.nodes[::-1][1:])
        else:
            # Gap between ways
            gaps = True
            if ndist2(nodes[-1], w.nodes[0]) < ndist2(nodes[-1], w.nodes[-1]):
                nodes.extend(w.nodes)
            else:
                nodes.extend(w.nodes[::-1])
    latlon = [[float(n.lat), float(n.lon)] for n in nodes]
    return (latlon, gaps)

# Former routes


def was_routes(mode="bus"):
    """Return a lineref:[urllist] dict of all was:route=<mode> routes in area.

    URLs point to the relations in OSM.
    """
    q = (f'[out:json][timeout:300];{area}\n'
         f'rel(area.hel)[type="was:route"]["was:route"="{mode}"]'
         f'[network~"HSL|Helsinki|Espoo|Vantaa"];out tags;')
    log.debug(q)
    rr = apiquery(q)
    refs = defaultdict(list)
    for r in rr.relations:
        if "ref" in r.tags.keys():
            refs[r.tags["ref"]].append(relid2url(r.id))
    return refs


def disused_routes(mode="bus"):
    """Return a lineref:[urllist] dict of all disused:route=<mode> routes in area.

    URLs points to the relations in OSM.
    """
    q = (f'[out:json][timeout:300];{area}\n'
         f'rel(area.hel)[type="disused:route"]["disused:route"="{mode}"]'
         f'[network~"HSL|Helsinki|Espoo|Vantaa"];out tags;')
    log.debug(q)
    rr = apiquery(q)
    refs = defaultdict(list)
    for r in rr.relations:
        if "ref" in r.tags.keys():
            refs[r.tags["ref"]].append(relid2url(r.id))
    return refs

# route_master relations


# ref -> route_master relation dict
overpy_route_master_dict = {
    k: None for k in list(stoptags.keys()) + ["minibus"]
}


def get_route_master_dict(mode, agency):
    """Return a (cached) ref: route_master dict for mode and network (i.e. agency).

    Only returns route_master relations with a ref.
    """
    if overpy_route_master_dict.get(mode, None):
        return overpy_route_master_dict[mode]
    q = (f'[out:json][timeout:300];'
         f'rel[type=route_master][route_master="{mode}"][network="{agency}"];'
         f'(._;>;>;);out body;')
    log.debug(q)
    rr = apiquery(q)
    rmd = defaultdict(list)
    for rel in rr.relations:
        ref = rel.tags.get("ref", None)
        if ref:
            rmd[ref].append(rel)
    overpy_route_master_dict[mode] = rmd
    return rmd


def route_master(route_ids):
    """Return route master relation from Overpass."""
    if not route_ids:
        log.error("route_master(): Empty route_ids list given")
        return []
    id_strs = ','.join(str(x) for x in route_ids)
    q = (f'[out:json][timeout:60];rel(id:{id_strs});'
         f'(rel(br)["type"="route_master"];);out body;')
    log.debug(q)
    rr = apiquery(q)
    return rr.relations


# Stops

def stoptags2mode(otags):
    """Return a list of mode strings which correspond to OSM tags."""
    outl = []
    for mode, mtaglist in stoptags.items():
        match = False
        for mtags in mtaglist:
            so_far_ok = True
            for k, v in mtags.items():
                if (k not in otags.keys()) or otags[k] != v:
                    so_far_ok = False
                    break
            if so_far_ok:
                match = True
                break
        if match:
            outl.append(mode)
    # train always matches also subway and monorail tags
    if ("train" in outl
            and ("subway" in outl or "monorail" in outl or "light_rail" in outl)):
        outl.remove("train")
    return outl


def mode2ovptags(mode, modetags=stoptags):
    """Return a list of Overpass tag filters."""
    tlist = modetags[mode]
    out = []
    for tags in tlist:
        ovp = ""
        for k, v in tags.items():
            if v is not None:
                ovp += '["{}"="{}"]'.format(k, v)
            else:
                ovp += '[!"{}"]'.format(k)
        if ovp:
            out.append(ovp)
    return out


def _sanitize_add(sd, rd, elist, etype, latlon=True, reftags=("ref",)):
    """Add elements in elist to defaultdict(list) sd or rd, depending on reftags.

    Add elements to sd if it has one of the tags in reftags, to dict rd if not.
    """
    for e in elist:
        dd = {
            "x:id": e.id,
            "x:type": etype,
        }
        if latlon:
            dd["x:latlon"] = member_coord(e)
        dd.update(e.tags)
        for tag in reftags:
            ref = e.tags.get(tag)
            if ref is not None:
                break
        if ref:
            # sd is defaultdict(list)
            sd[ref].append(dd)
        else:
            # rd is dict
            rd[e.id] = dd


def _format_stops(rr):
    """Sort overpass query result rr into a stop info dict."""
    refstops = defaultdict(list)
    rest = {}
    # NB: Because _sanitize_add() calls member_coords(), which gets more
    # (untagged) nodes (and maybe ways), the order of calls below
    # must be like this.
    _sanitize_add(refstops, rest, rr.nodes, "n")
    _sanitize_add(refstops, rest, rr.ways, "w")
    _sanitize_add(refstops, rest, rr.relations, "r")
    return refstops, rest


def stops_by_refs(refs, mode=None, full=False):
    """Return a list of stops from OSM with 'ref' tag in refs.

    The default (mode=None) is to query all known modes, but a list of
    mode strings or a single mode string can be given in the mode arg.

    The return value is a list of (element_type, id) tuples.
    If 'full' is True, a ref: tags dict is returned, see stops()
    for details.
    """
    if mode is None:
        qlist = [e for m in stoptags.keys() for e in mode2ovptags(m)]
    elif isinstance(mode, list):
        qlist = [e for m in mode for e in mode2ovptags(m)]
    else:
        qlist = mode2ovptags(mode)
    refpat = "|".join(str(r) for r in refs)
    q = stopref_area + "(\n"
    for st in qlist:
        q += 'nwr(area.hel){}[ref~"({})"];\n'.format(st, refpat)
    q += "); out body;"
    log.debug(q)
    rr = apiquery(q)
    if full:
        return _format_stops(rr)
    stopids = []
    for ref in refs:
        ids = []
        ids.extend(("node", e.id) for e in rr.nodes if e.tags["ref"] == ref)
        ids.extend(("way", e.id) for e in rr.ways if e.tags["ref"] == ref)
        ids.extend(("relation", e.id)
                   for e in rr.relations if e.tags["ref"] == ref)
        if not ids:
            log.warning(f"OSM stop object not found for ref '{ref}'")
        elif len(ids) > 1:
            log.warning("More than OSM object for ref '%s': %s" % (
                ref, ', '.join(str(i) for i in ids)))
        stopids.extend(ids)
    return stopids


def stops(mode="bus"):
    """Return all stops for a given mode in the area.

    The mode can also be a list of mode strings, in which case stops for all
    the modes listed are returned.

    A dict with ref as a key and tags dictionaries as values is returned.
    The following additional keys are included in the tag dictionaries:
    x:id        id of the object
    x:type      type if the object as a string of length 1 ('n', 'w', or 'r')
    x:latlon    (latitude, longitude) tuple of the object, as calculated
                by osm.member_coord()
    """
    modetags = stoptags
    modetags.update({'citybike': citybiketags})

    qtempl = "  nwr(area.hel){};"
    if isinstance(mode, list):
        qlist = [e for m in mode for e in mode2ovptags(m, modetags=modetags)]
    else:
        qlist = mode2ovptags(mode, modetags=modetags)

    q = (f"[out:json][timeout:240];\n{area}\n(\n"
         + "\n".join([qtempl.format(t) for t in qlist]) + "\n);out body;")
    log.debug(q)
    rr = apiquery(q)
    refstops, rest = _format_stops(rr)
    return refstops, rest


# FIXME: Does not work when lifetime prefixes are mixed in tags,
# e.g. 'disused:highway=bus_stop' and 'was:ref=H1234'
def old_stops(mode, prefix="was:"):
    """Return stops from a query formed by adding prefix to stoptags."""
    mtags = citybiketags if mode == 'citybike' else stoptags[mode]

    taglist = [[k + '"="' + v + '"' for k, v in td.items()]
               for td in mtags]
    preftags = []
    for tags in taglist:
        head = '"' + prefix + tags[0]
        tail = tags[1:]
        # Make permutations of tags in tail with and without prefix
        ptup = tuple(itertools.repeat(['', prefix], len(tail)))
        preflist = list(itertools.product(*ptup))
        pacc = [[head] + ['"' + s[0] + s[1] for s in zip(pl, tail)] for pl in preflist]
        preftags.append(pacc)

    qlist = []
    for tags in preftags:
        for permutations in tags:
            qlist.append("".join("[{}]".format(t) for t in permutations))

    q = "[out:json][timeout:120];\n" + area + "\n(\n" \
        + "\n".join(["  nwr(area.hel){};".format(t) for t in qlist])\
        + "\n);out tags;"
    log.debug(q)
    rr = apiquery(q)

    refstops = defaultdict(list)
    refless = {}
    reftags = ("ref", prefix + "ref")
    # NB: Because _sanitize_add() calls member_coords(), which gets more
    # (untagged) nodes (and maybe ways), the order of calls below
    # must be like this.
    _sanitize_add(
        refstops, refless, rr.nodes, "n", latlon=False, reftags=reftags)
    _sanitize_add(
        refstops, refless, rr.ways, "w", latlon=False, reftags=reftags)
    _sanitize_add(
        refstops, refless, rr.relations, "r", latlon=False, reftags=reftags)
    # Add non-prefixed tags so that they can be matched
    for stops in refstops.values():
        for stop in stops:
            newd = {}
            for k, v in stop.items():
                if k.startswith(prefix):
                    newk = k.replace(prefix, '', 1)
                    newd[newk] = v
            stop.update(newd)

    return refstops, refless


def citybikes():
    return stops(mode="citybike")


def old_citybikes(prefix="was:"):
    return old_stops(mode="citybike", prefix=prefix)


def stations(mode="bus"):
    """Return all stations for a given mode in the area.

    The mode can also be a list of mode strings, in which case stations for
    all the modes listed are returned.

    A list with tags dictionaries as values is returned.
    The following additional keys are included in the tag dictionaries:
    x:id    id of the object
    x:type  type if the object as a string of length 1 ('n', 'w', or 'r')
    x:latlon (latitude, longitude) tuple of the object, as calculated
            by osm.member_coord()
    """
    qtempl = 'nwr(area.hel){};'
    if isinstance(mode, list):
        qlist = [e for m in mode for e in mode2ovptags(m, stationtags)]
    else:
        qlist = mode2ovptags(mode, stationtags)

    q = (f'[out:json][timeout:120];\n{area}\n(\n'
         + "\n".join([qtempl.format(t) for t in qlist]) + '\n);out body;')
    log.debug(q)
    rr = apiquery(q)

    def sanitize_addlist(sl, elist, etype):
        for e in elist:
            dd = {
                "x:id": e.id,
                "x:type": etype,
                "x:latlon": member_coord(e),
            }
            dd.update(e.tags)
            sl.append(dd)

    stations = []
    # NB: Because sanitize_add() calls member_coords(), which gets more
    # (untagged) nodes (and maybe ways), the order of calls below
    # must be like this.
    sanitize_addlist(stations, rr.nodes, "n")
    sanitize_addlist(stations, rr.ways, "w")
    sanitize_addlist(stations, rr.relations, "r")
    return stations
