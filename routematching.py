# Copyright (C) 2018-2023 Teemu Ikonen <tpikonen@gmail.com>

# This file is part of Taival.
# Taival is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License version 3 as published by the
# Free Software Foundation. See the file COPYING for license text.

import logging

from util import ldist2

log = logging.getLogger(__name__)


def match_stopcount(plist, stopcount):
    """Return a list of pattern codes with stop count closest to 'stopcount'."""
    out = []
    mindelta = 1e6
    for code, p in plist:
        delta = abs(len(p["stops"]) - stopcount)
        log.debug("code {}, delta = {}".format(code, delta))
        if delta < mindelta:
            out = [code]
            mindelta = delta
        elif delta == mindelta:
            out.append(code)

    return out


def match_shapes_by_endpoints(shapes1, shapes2):
    """Determine a mapping from one set of shapes to another.

    The mapping is based on geometry. Return permutation indices for both
    directions.
    """
    if len(shapes1) > len(shapes2):
        swap = True
        s1 = shapes2
        s2 = shapes1
    else:
        swap = False
        s1 = shapes1
        s2 = shapes2
    m1to2 = [None] * len(s1)
    m2to1 = [None] * len(s2)
    for i in range(len(s1)):
        if not s1[i]:  # Handle empty shape
            m1to2[i] = 0
            continue
        mind = 1.0e10
        minind = 0
        cbeg = s1[i][0]
        cend = s1[i][-1]
        for j in range(len(s2)):
            if not s2[j]:  # Handle empty shape
                continue
#            d = min(ldist2(cbeg, s2[j][0]), ldist2(cend, s2[j][-1]))
            d = ldist2(cbeg, s2[j][0]) + ldist2(cend, s2[j][-1])
            if d < mind:
                mind = d
                minind = j
        m1to2[i] = minind
    for i in range(len(m1to2)):
        m2to1[m1to2[i]] = i
#    if any(v is None for v in m1to2 + m2to1):
#        print("(mapping is not a bijection)")
    if swap:
        return (m2to1, m1to2)
    else:
        return (m1to2, m2to1)
