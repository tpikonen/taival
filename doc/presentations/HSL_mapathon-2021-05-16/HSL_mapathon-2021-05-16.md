# Taival - Joukkoliikenteen kartoitustyökalu

---
# Verkkosivut

 * Vertailu OSM-wikissä: https://wiki.osm.org/wiki/Finland:Joukkoliikenne/HSL#Vertailu
 * Koodi: https://codeberg.org/tpikonen/taival

---
# Motivaatio

 * Alunperin: Joukkoliikenteen reittien ja pistekohteiden kartoitus OSM:iin on työlästä, mutta avointa dataa on runsaasti saatavilla.
 * Voisiko avointa dataa muokata ohjelmallisesti helpommin hyödynnettävään muotoon?
 * (Vastaus: ehkä)

---
# Motivaatio

 * Nyt: (HSL:n) Avoimen datan ja OSM-datan vertailu ja raportointi
 * Miksi:
    - Puuttuva data löytyy yhdestä paikasta ('pehmeä importti')
    - Reitit/pysäkit/asemat menevät rikki, kukaan ei huomaa

---
# Taival: Toiminnot

 1. OSM:n joukkoliikennereittien tekeminen HSL:n datan avulla
 2. Reittien, pysäkkien, asemien ja kaupunkipyöräasemien vertailu OSM vs. HSL

---
# Reitit, gpx-jälki

    $ taival gpx 1 tram
    $ ls 1*.gpx
    1_osm_0.gpx
    1_osm_1.gpx
    1_HSL_HSL:1001:0:02_0.gpx
    1_HSL_HSL:1001:0:01_0.gpx
    1_HSL_HSL:1001:0:03_0.gpx
    1_HSL_HSL:1001:1:03_1.gpx
    1_HSL_HSL:1001:1:02_1.gpx
    1_HSL_HSL:1001:1:05_1.gpx
    1_HSL_HSL:1001:1:04_1.gpx
    1_HSL_HSL:1001:1:01_1.gpx

---
# Reitit, gpx-jälki
![gpx](1_tram.png)

---
# Reitit, OSM-XML

    $ taival osmxml 1 tram
    $ ls 1*.osm
    1_HSL_HSL:1001:0:01.osm
    1_HSL_HSL:1001:0:02.osm
    1_HSL_HSL:1001:0:03.osm
    1_HSL_HSL:1001:1:01.osm
    1_HSL_HSL:1001:1:02.osm
    1_HSL_HSL:1001:1:03.osm
    1_HSL_HSL:1001:1:04.osm
    1_HSL_HSL:1001:1:05.osm

---
# Reitit, OSM-XML

    $ cat 1_HSL_HSL:1001:0:01.osm
    <member type='node' ref='313998777' role='platform' />
    <member type='node' ref='655131553' role='platform' />
    ...
    <member type='node' ref='298168466' role='platform' />
    <tag k='name' v='1 Eira–Töölö–Sörnäinen (M)–Käpylä' />
    <tag k='ref' v='1' />
    <tag k='network' v='HSL' />
    <tag k='route' v='tram' />
    <tag k='type' v='route' />
    <tag k='from' v='Eira' />
    <tag k='to' v='Käpylä' />
    <tag k='public_transport:version' v='2' />

---
# Reittien tekemisestä

 * Unohda (lähes) kaikki 'public_transport' tagit
 * Refined public transport proposal:

wiki.osm.org/wiki/Proposed_features/Refined_Public_Transport

---
# Vertailu

## Kaksi vaihetta:

 1. Datan haku
 2. Raporttien generointi

---
# Vertailu, datan haku

    $ taival collect ferry
    $ taival collect subway
    $ taival collect train
    $ taival collect tram
    $ taival collect bus
    $ taival collect stops
    $ taival collect stations
    $ taival collect citybikes
    $ ls *.pickle
    HSL_bus.pickle
    HSL_citybikes.pickle
    HSL_ferry.pickle
    HSL_stations.pickle
    HSL_stops.pickle
    HSL_subway.pickle
    HSL_train.pickle
    HSL_tram.pickle

---
# Vertailu, raportointi

    $ taival routes HSL_tram.pickle > HSL_tram.txt
    $ # Siirretään OSM-wikiin pywikibot-skriptillä
    $ upload.sh HSL_tram.txt "Update"

---
# Wiki-raportti / Demo

![Ratikkalinjat](tramlines.png)
https://wiki.osm.org/wiki/Finland:Joukkoliikenne/HSL/tram

---
# Wiki-raportti / Demo

![Ratikkapysäkit](tramstops.png)
wiki.osm.org/wiki/Finland:Joukkoliikenne/HSL/stops/tram

---
# Toteutus

 * Python 3
 * Kirjastot:
    - requests
    - overpy
    - gpxpy
    - pykdtree
 * HSL-datat Digitransitista (GraphQL)
 * OSM-datat Overpassista (Overpass QL)
 * Lisenssi: AGPL3

---
# Ongelmia

 * Hidas (Overpass ei pidä toistuvista kyselyistä)
 * Mediawiki rajoittaa raportin ulkoasua

---
# Tulevaisuus

 * Raportit muista digitransit-alueista
 * Digitransit -> GTFS
 * Overpass -> OSM API

---
# Valmis!
