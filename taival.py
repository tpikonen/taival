#!/usr/bin/env python3

# Copyright (C) 2018-2023 Teemu Ikonen <tpikonen@gmail.com>

# This file is part of Taival.
# Taival is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License version 3 as published by the
# Free Software Foundation. See the file COPYING for license text.

import argparse
import datetime
import logging
import os
import pickle
import sys
from collections import defaultdict

import gpxpy.gpx

import digitransit
import hsl
import mediawiki as mw
import osm

# from routematching import match_shapes_by_endpoints,

from util import (
    arrivals2intervals,
    csv2dict,
    ddl_uniq_key_merge,
    linesortkey,
)

pvd = None

osm.area = hsl.overpass_area
osm.stopref_area = hsl.overpass_stopref_area

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d| %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S")

log = logging.getLogger(__name__)


def write_gpx(latlon, fname, waypoints=[]):
    gpx = gpxpy.gpx.GPX()
    if latlon:
        gpx_track = gpxpy.gpx.GPXTrack()
        gpx.tracks.append(gpx_track)
        gpx_segment = gpxpy.gpx.GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)
        for ll in latlon:
            gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(ll[0], ll[1]))
    for w in waypoints:
        gpx.waypoints.append(gpxpy.gpx.GPXWaypoint(w[0], w[1], name=w[2],
                                                   description=w[3]))
    with open(fname, "w") as ff:
        ff.write(gpx.to_xml())


def digitransit2gpx(dt, mode, otype, refs):
    """Write gpx files for given refs from digitransit API data."""
    if otype == "route":
        for ref in refs:
            log.debug("Calling dt.codes_query")
            codes = dt.codes_query(ref, mode)
            for c in codes:
                log.debug("    Calling dt.shape_query")
                (dirid, latlon) = dt.shape_query(c, mode)
                log.debug("    Calling dt.platforms_query")
                stops = dt.platforms_query(c)
                fname = "%s_%s_%s_%d.gpx" % (ref, dt.agency, c, dirid)
                write_gpx(latlon, fname, waypoints=stops)
                print(fname)
            if not codes:
                log.error(f"Line '{ref}' not found in {dt.agency}.")
    elif otype == "stop":
        wpts = []
        for ref in refs:
            modestops, clusters = dt.stops(name=ref)
            s = modestops.get(mode, {}).get(ref, None)
            if s is not None:
                wpts.append(
                    (s["latlon"][0], s["latlon"][1], ref, s['name']))
        if not wpts:
            log.warning(f"No stops found from {dt.agency}")
            return
        wpts.sort(key=lambda s: linesortkey(s[2]))
        if len(wpts) == 1:
            rname = wpts[0][2]
        else:
            rname = "{}_to_{}".format(wpts[0][2], wpts[-1][2])
        fname = f"{rname}_{dt.agency}.gpx"
        write_gpx([], fname, waypoints=wpts)
        print(fname)
    else:
        log.error("Unknown type '{otype}'")


def osm2gpx(mode, otype, refs):
    if otype == 'route':
        for ref in refs:
            log.debug(f"Calling osm.rels_query({ref}, {mode})")
            rels = osm.rels_query(ref, mode)
            if len(rels) > 0:
                for i, rel in enumerate(rels):
                    fname = "%s_osm_%d.gpx" % (ref, i)
                    log.debug("Calling osm.shape")
                    latlon = osm.route_shape(rel)[0]
                    log.debug("Calling osm.platforms")
                    waypts = osm.route_platforms_or_stops(rel)
                    write_gpx(latlon, fname, waypoints=waypts)
                    print(fname)
            else:
                log.warning("Line '{ref}' not found in OSM.")
    elif otype == 'stop':
        log.debug(f"Calling osm.stops_by_refs with mode {mode}")
        stops, rest = osm.stops_by_refs(refs, mode=mode, full=True)
        srefs = sorted(stops.keys(), key=linesortkey)
        if len(srefs) < 1:
            log.warning("No stops found from OSM")
            return
        if len(srefs) == 1:
            rname = srefs[0]
        else:
            rname = "{}_to_{}".format(srefs[0], srefs[-1])
        fname = f"{rname}_osm.gpx"
        waypoints = []
        for ref in srefs:
            stopl = stops[ref]
            waypoints.extend((s["x:latlon"][0], s["x:latlon"][1], ref,
                              s.get('name', '')) for s in stopl)
        write_gpx([], fname, waypoints=waypoints)
        print(fname)
    else:
        log.error("Unknown type '{otype}'")


def collect_interval_tags(code):
    """Return interval tags for a pattern code determined from HSL data.

    Return interval tags for peak and normal hours for weekdays (monday),
    saturday and sunday. Intervals are converted from arrival data.
    """
    # Get interval tags from API
    today = datetime.date.today()
    delta = (5 + 7 - today.weekday()) % 7  # Days to next saturday
    tags = {}
    daynames = ["saturday", "sunday", None]  # weekdays (monday) is the default
    for i in range(3):
        day = (today + datetime.timedelta(days=delta + i)).strftime("%Y%m%d")
        log.debug("Calling pvd.arrivals_for_date()")
        (norm, peak, night) = arrivals2intervals(
            pvd.arrivals_for_date(code, day), pvd.peakhours, pvd.nighthours)
        tagname = "interval" + (":" + daynames[i] if daynames[i] else "")
        if (len(norm) + len(peak) + len(night)) == 0:
            tags[tagname] = "no_service"
        else:
            # FIXME: Maybe combine norm and peak if peak is short enough?
            # Only tag if there are more than 2 intervals, i.e.
            # at least 3 arrivals in a period.
            if len(norm) > 1:
                med_norm = norm[len(norm) // 2]
                tags[tagname] = str(med_norm)
                if len(peak) > 1:
                    med_peak = peak[len(peak) // 2]
                    # Only add interval:peak tag if it's significantly smaller.
                    if med_peak <= (0.8 * med_norm):
                        tags[tagname + ":peak"] = str(med_peak)
            if len(night) > 1:
                tags[tagname + ":night"] = str(night[len(night) // 2])
                if not norm:
                    tags[tagname] = "no_service"
    itmp = tags.get("interval", "1")
    interval = int(itmp) if itmp.isdigit() else 1
    itmp = tags.get("interval:saturday", "1")
    isat = int(itmp) if itmp.isdigit() else 1
    itmp = tags.get("interval:sunday", "1")
    isun = int(itmp) if itmp.isdigit() else 1
    itmp = tags.get("interval:night", "1")
    inight = int(itmp) if itmp.isdigit() else 1
    itmp = tags.get("interval:saturday:night", "1")
    isatnight = int(itmp) if itmp.isdigit() else 1
    itmp = tags.get("interval:sunday:night", "1")
    isunnight = int(itmp) if itmp.isdigit() else 1
    # Remove weekend tags if they are not significantly different
    if (abs(isat - interval) / interval < 0.2
            and tags.get("interval:saturday", "") != "no_service"):
        tags.pop("interval:saturday", 0)
        tags.pop("interval:saturday:peak", 0)
    if (abs(isun - interval) / interval < 0.2
            and tags.get("interval:sunday", "") != "no_service"):
        tags.pop("interval:sunday", 0)
        tags.pop("interval:sunday:peak", 0)
    if (abs(isatnight - inight) / inight < 0.2
            and tags.get("interval:saturday:night", "") != "no_service"):
        tags.pop("interval:saturday:night", 0)
    if (abs(isunnight - inight) / inight < 0.2
            and tags.get("interval:sunday:night", "") != "no_service"):
        tags.pop("interval:sunday:night", 0)
    return tags


def collect_line(lineref, mode, agency, networks, interval_tags=False):
    """Report on differences between OSM and HSL data for a given line."""
    ld = {}  # line dict
    ld["lineref"] = lineref
    ld["mode"] = mode
    ld["agency"] = agency
    log.debug(f"Processing {mode} line {lineref}")

    rels = osm.rels(lineref, mode, networks)
    osmroutes = {}
    for rel in rels:
        geometry, gaps = osm.route_shape(rel)
        osmroutes[rel.id] = {
            "tags": rel.tags,
            # member roles are needed for e.g. PTv1 test
            "member_roles": [mem.role for mem in rel.members],
            "platforms_and_stops": osm.route_platforms_and_stops(rel),
            "gaps": gaps,
            "geometry": geometry,
        }

    if len(osmroutes) < 1:
        log.debug("No route relations found in OSM.\n")
        return ld

    ld["osmroutes"] = osmroutes
    log.debug(
        "Found OSM route ids: %s" %
        (", ".join("[%s %d]" % (osm.relid2url(rid), rid) for rid in osmroutes.keys())))

    rmd = osm.get_route_master_dict(mode, agency)
    ld["osmroutemasters"] = {
        rm.id: {
            "members": [m.ref for m in rm.members],
            "tags": rm.tags,
        } for rm in rmd[lineref]
    }

    log.debug("Calling pvd.route")
    proute = pvd.routes(lineref, mode)
    ld["pvdtags"] = {
        "shortName": proute["shortName"],
        "longName": proute["longName"],
        "mode": proute["mode"],
        "type": proute["type"],
        "gtfsId": proute["gtfsId"],
    }
    pvdpatterns = {
        pat["code"]: {
            "directionId": pat["directionId"],
            # stops should be a (lat, lon, ref, name)
            # like osmroutes['platforms_and_stops']
            "stops": pat["stops"],
            "geometry": [(lalo["lat"], lalo["lon"]) for lalo in pat["geometry"]],
        }
        for pat in proute['patterns']
    }
    ld["pvdpatterns"] = pvdpatterns

    if interval_tags:
        ld["interval_tags"] = {code: collect_interval_tags(code)
                               for code in pvdpatterns.keys()}

    log.debug(f"Line {lineref} done\n")

    return ld


def collect_routes(mode="bus", interval_tags=False):
    """Collect data for a given mode from APIs.

    Call collect_line for all discovered lines.
    """
    log.info("Starting collect %s" % mode)
    md = {}
    md["mode"] = mode
    md["shapetol"] = pvd.shapetols[mode]
    md["interval_tags"] = interval_tags
    agency = pvd.agency
    agencyurl = pvd.agencyurl
    md["agency"] = agency
    md["agencyurl"] = agencyurl
    md["modecolors"] = pvd.modecolors

    networks = [agency] + hsl.cities + [None]
    if agency == 'HSL':
        networks.append("Saaristoliikenne")
    osmdict = osm.all_linerefs(mode, networks)
    if mode == 'tram' and agency == 'HSL':
        osmdict.update(osm.all_linerefs('light_rail', networks))
    hsldict = pvd.all_linerefs(mode)
    refless = osm.rels_refless(mode)
    refless = [r for r in refless if r.tags.get("network", None) in networks]
    wasroutes = osm.was_routes(mode)
    disroutes = osm.disused_routes(mode)
    md["osmdict"] = osmdict
    md["hsldict"] = hsldict
    md["refless"] = refless
    md["wasroutes"] = wasroutes
    md["disroutes"] = disroutes
    md["networks"] = networks

    if mode == "bus":
        hsl_localbus = pvd.taxibus_linerefs(mode)
        md["hsl_localbus"] = hsl_localbus
        osm_minibusdict = osm.all_linerefs("minibus", agency)
        md["osm_minibusdict"] = osm_minibusdict

    hsllines = set(hsldict)
    hsllines = list(hsllines)
    hsllines.sort(key=linesortkey)
    lines = {}
    for line in hsllines:
        ld = collect_line(line, mode, agency, networks, interval_tags)
        # Missing tram lines are maybe light_rail lines
        if mode == "tram" and "osmroutes" not in ld:
            ld = collect_line(line, "light_rail", agency, networks, interval_tags)
        lines[line] = ld
    md["lines"] = lines
    log.info("Done: collect %s" % mode)
    return md


def collect_stops():
    log.info("Starting collect stops")
    osmstops = defaultdict(list)
    osmrest = {}
    oldstops = defaultdict(list)
    oldrest = {}

    def collect_stops_mode(mode):
        log.debug('Calling osm.stops("{}")'.format(mode))
        refstops, rest = osm.stops(mode)
        ddl_uniq_key_merge(osmstops, refstops, "x:id")
        osmrest.update(rest)

        log.debug('Calling osm.oldstops("{}", prefix="was:")'.format(mode))
        oldrefstops, oldrefless = osm.old_stops(mode, prefix="was:")
        ddl_uniq_key_merge(oldstops, oldrefstops, "x:id")
        oldrest.update(oldrefless)

        log.debug('Calling osm.oldstops("{}", prefix="disused:")'.format(mode))
        oldrefstops, oldrefless = osm.old_stops(mode, prefix="disused:")
        ddl_uniq_key_merge(oldstops, oldrefstops, "x:id")
        oldrest.update(oldrefless)

    train_cities = [
        "Helsinki",
        "Espoo",
        "Hyvinkää",
        "Iitti",
        "Järvenpää",
        "Kauniainen",
        "Kerava",
        "Kirkkonummi",
        "Kouvola",
        "Lahti",
        "Mäntsälä",
        "Orimattila",
        "Riihimäki",
        "Siuntio",
        "Tuusula",
        "Vantaa"]
    for city in train_cities:
        log.debug("Collecting train stops for %s" % city)
        osm.area = hsl.get_overpass_area([city])
        collect_stops_mode("train")

    osm.area = hsl.get_overpass_area(["Helsinki"])
    collect_stops_mode("ferry")

    osm.area = hsl.get_overpass_area(["Helsinki", "Espoo"])
    collect_stops_mode("tram")
    collect_stops_mode("light_rail")
    collect_stops_mode("subway")

    osm.area = hsl.get_overpass_area(hsl.cities + hsl.extracities_bus)
    collect_stops_mode("bus")

    osm.area = hsl.overpass_area

    log.debug('Calling pvd.stops()')
    pst = pvd.stops()

    hsl.normalize_helsinki_codes(osmstops)
    for pmode in pst.values():
        hsl.normalize_helsinki_codes(pmode, change_code=True)

    for mdict in pst.values():
        for ps in mdict.values():
            namecount = len([
                True for d in pst.values() for s in d.values()
                if s["name"] == ps["name"]])
            ps["namecount"] = namecount

    sd = {
        "ost": osmstops,
        "rst": oldrest,
        "oldstops": oldstops,
        "oldrest": oldrest,
        "pst": pst,
        "agency": pvd.agency,
    }
    log.info("Done: collect stops")

    return sd


def collect_stations():
    log.info("Starting collect stations")
    pstat = pvd.stations()
    osm.area = hsl.get_overpass_area(hsl.cities + hsl.extracities_stations)
    ostat = {mode: osm.stations(mode) for mode in pstat.keys()}
    osm.area = hsl.overpass_area
    log.info("Done: collect stations")
    return {
        "ostat": ostat,
        "pstat": pstat,
        "agency": pvd.agency
    }


def collect_citybikes():
    log.info("Starting collect citybikes")
    datadir = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),
                           hsl.datadir)
    csvd_he = csv2dict(os.path.join(
        datadir, "Helsingin_ja_Espoon_kaupunkipyöräasemat-2022-08-11.csv"),
        "ID")
    csvd_v = csv2dict(os.path.join(
        datadir, "Vantaan_kaupunkipyöräasemat-2022-11-05.csv"), "ID")
    pcbs = pvd.citybikes()
    # Add info from CSV data
    for k in csvd_he.keys():
        if k in pcbs.keys():
            # pcbs[k]["capacity"] = csvd_he[k]["Kapasiteet"].strip()
            # pcbs[k]["name"] = csvd_he[k]["Nimi"].strip()
            # pcbs[k]["name:fi"] = csvd_he[k]["Nimi"].strip()
            pcbs[k]["name:en"] = csvd_he[k]["Name"].strip()
            pcbs[k]["name:sv"] = csvd_he[k]["Namn"].strip()
            # pcbs[k]["network"] = "Helsinki"
    for k in csvd_v.keys():
        if k in pcbs.keys():
            pcbs[k]["capacity"] = csvd_v[k]["Kapasiteet"].strip()
            pcbs[k]["name"] = csvd_v[k]["Nimi"].strip()
            pcbs[k]["name:fi"] = csvd_v[k]["Nimi"].strip()
            pcbs[k]["name:en"] = csvd_v[k]["Name"].strip()
            pcbs[k]["name:sv"] = csvd_v[k]["Namn"].strip()
#            pcbs[k]["network"] = "Vantaa"

    prev_area = osm.area
    osm.area = hsl.get_overpass_area(["Helsinki", "Espoo", "Vantaa"])

    refcbs, rest = osm.citybikes()

    old_cbs = defaultdict(list)
    old_rest = {}
    old_prefixes = ["was:", "disused:", "removed:", "demolished:"]
    for prefix in old_prefixes:
        old, orefless = osm.old_citybikes(prefix=prefix)
        ddl_uniq_key_merge(old_cbs, old, "x:id")
        old_rest.update(orefless)

    osm.area = prev_area

    log.info("Done: collect citybikes")
    return {
        "osm_cbs": refcbs,
        "osm_cbs_rest": rest,
        "old_cbs": old_cbs,
        "old_cbs_prefixes": old_prefixes,
        "pvd_cbs": pcbs,
        "agency": pvd.agency,
    }


def sub_gpx(args):
    log.info("Starting gpx, mode %s, type %s, refs '%s'" % (
        args.mode, args.type, args.refs))
    osm2gpx(args.mode, args.type, args.refs)
    digitransit2gpx(pvd, args.mode, args.type, args.refs)
    log.info("Done: gpx, mode %s, type %s, refs '%s'" % (
        args.mode, args.type, args.refs))


def write_route_xml(fname, platforms, htags, mode, reverse=False):
    with open(fname, "w") as ff:
        # ff.write(
        # "<?xml version='1.0' encoding='UTF-8'?>\n"
        # "<osm version='0.6' generator='taival'>\n\n")
        # ff.write("  <relation>\n")
        for ptype, pid in platforms:
            ff.write(
                "    <member type='{}' ref='{}' role='platform' />\n"
                .format(ptype, pid))
        stopnames = hsl.longname2stops(htags["longName"])
        if reverse:
            stopnames.reverse()
        ff.write("    <tag k='name' v='%s' />\n" % (
            htags["shortName"] + " " + "–".join(stopnames)))
        ff.write("    <tag k='ref' v='%s' />\n" % htags["shortName"])
        ff.write("    <tag k='network' v='HSL' />\n")
        ff.write("    <tag k='route' v='%s' />\n" % mode)
        ff.write("    <tag k='type' v='route' />\n")
        ff.write(f"    <tag k='from' v='{stopnames[0]}' />\n")
        ff.write(f"    <tag k='to' v='{stopnames[-1]}' />\n")
        ff.write("    <tag k='public_transport:version' v='2' />\n")
#        ff.write("  </relation>\n\n")
#        ff.write("</osm>\n")


def sub_josm(args):
    log.info("Starting josm, mode %s, type %s, refs '%s'" % (
        args.mode, args.type, args.refs))

    if args.type == 'route':
        ref = args.refs[0]
        if len(args.refs) > 1:
            log.warning(
                f"Multiple refs for route not supported (yet). Using {ref}")
        # log.debug("Calling osm.rels_query")
        # rels = osm.rels_query(ref, args.mode)
        # osmshapes = [osm.route_shape(rel)[0] for rel in rels]

        log.debug("Calling pvd.codes_query")
        codes = pvd.codes_query(ref, args.mode)
        log.debug("Calling pvd.tags_query")
        htags = pvd.tags_query(ref, args.mode)
        # pvdshapes = [pvd.shape_query(c, args.mode)[1] for c in codes]

        # osm2pvd, pvd2osm = match_shapes_by_endpoints(osmshapes, pvdshapes)

        for c in codes:
            log.debug("Pattern code %s" % c)
            # reverse stops string if direction code is odd
            reverse = (int(c.split(":")[2]) % 2) == 1
            log.debug("   Calling pvd.platforms_query")
            stops = [p[2] for p in pvd.platforms_query(c)]
            fname = "%s_%s_%s.osm" % (ref, pvd.agency, c)
            log.debug("   Calling osm.stops_by_refs")
            ids = osm.stops_by_refs(stops, args.mode)
            write_route_xml(fname, ids, htags, args.mode, reverse)
            print(fname)
        if not codes:
            print("Line '%s' not found in %s for mode %s." % (
                ref, pvd.agency, args.mode))
    elif args.type == 'stop':
        log.error("Output for stop not implemented yet.")
        pass
    else:
        log.error("Unknown type '{otype}'")

    log.info("Done: josm, mode %s, type %s, refs '%s'" % (
        args.mode, args.type, args.refs))


def get_output(args):
    if args.output == '-':
        out = sys.stdout
    else:
        out = open(args.output, "w")
    return out


def sub_collect(args):
    if not args.output:
        args.output = "{}_{}.pickle".format(pvd.agency, args.mode)
    if args.mode == 'stops':
        d = collect_stops()
    elif args.mode == 'stations':
        d = collect_stations()
    elif args.mode == 'citybikes':
        d = collect_citybikes()
    elif args.mode in osm.stoptags.keys():
        d = collect_routes(mode=args.mode, interval_tags=args.interval_tags)
    else:
        log.error("argument not recognized after 'collect'")
        return
    with open(args.output, "wb") as out:
        pickle.dump(d, out)


def sub_routes(args):
    with open(args.file, 'rb') as f:
        d = pickle.load(f)
    if "mode" in d.keys():
        out = get_output(args)
        mw.outfile = out
        mw.report_routes(d)
        if out and out != sys.stdout:
            out.close()
    else:
        log.error("Unrecognized route dictionary from pickle file.")


def sub_stops(args):
    city = None
    mode = None
    if args.filt1 in hsl.city2prefixes.keys():
        city = args.filt1
    elif args.filt1 in osm.stoptags.keys():
        mode = args.filt1

    if args.filt2 in hsl.city2prefixes.keys():
        if city:
            log.error("More than one city filters not supported")
            return
        else:
            city = args.filt2
    elif args.filt2 in osm.stoptags.keys():
        if mode:
            log.error("More than one mode filters not supported")
            return
        else:
            mode = args.filt2

    if not args.input:
        args.input = "{}_stops.pickle".format(pvd.agency)
    log.debug("Stops input: '{}', mode: {}, city {}".format(
        args.input, mode, city))
    with open(args.input, 'rb') as f:
        d = pickle.load(f)
    if "ost" in d.keys() and "pst" in d.keys():
        out = get_output(args)
        mw.outfile = out
        mw.report_stops(d, mode=mode, city=city)
        if out and out != sys.stdout:
            out.close()
    else:
        log.error("Incompatible pickle file")


def sub_stations(args):
    if args.filt1 in osm.stationtags.keys():
        mode = args.filt1
    elif args.filt1 is None:
        mode = None
    else:
        log.error("Unrecognized transport mode '{}'.".format(args.filt1))
        sys.exit(1)

    if not args.input:
        args.input = "{}_stations.pickle".format(pvd.agency)
    log.debug("Stations input: '{}', mode: {}".format(args.input, mode))
    with open(args.input, 'rb') as f:
        d = pickle.load(f)
    if "ostat" in d.keys() and "pstat" in d.keys():
        out = get_output(args)
        mw.outfile = out
        mw.report_stations(d, mode=mode)
        if out and out != sys.stdout:
            out.close()
    else:
        log.error("Incompatible pickle file")
        sys.exit(2)


def sub_citybikes(args):
    if not args.input:
        args.input = "{}_citybikes.pickle".format(pvd.agency)
    log.debug("Citybikes input: '{}'".format(args.input))
    with open(args.input, 'rb') as f:
        d = pickle.load(f)
    if "osm_cbs" in d.keys() and "pvd_cbs" in d.keys():
        out = get_output(args)
        mw.outfile = out
        mw.report_citybikes(d)
        if out and out != sys.stdout:
            out.close()
    else:
        log.error("Incompatible pickle file")
        sys.exit(2)


def main():
    global pvd
    parser = argparse.ArgumentParser()
    parser.add_argument('--version', '-v', action='version', version='0.0.1')
    parser.set_defaults(func=lambda _: print(parser.format_usage()))
    subparsers = parser.add_subparsers(help='sub-command -h for help')

    parser_gpx = subparsers.add_parser(
        'gpx', help='Output gpx files for given object.')
    parser_gpx.add_argument(
        'mode', metavar='<mode>', choices=osm.stoptags.keys(),
        help="Transit mode: 'train', 'subway', 'tram', 'bus' or 'ferry'.")
    parser_gpx.add_argument(
        'type', metavar='<type>', choices=['route', 'stop'],
        help="Object type ('route', or 'stop')")
    parser_gpx.add_argument(
        'refs', metavar='<ref>', nargs='+',
        help="One or more object references (e.g. '572', 'H1234')")
    parser_gpx.set_defaults(func=sub_gpx)

    parser_josm = subparsers.add_parser(
        'josm',
        help='Output JOSM XML snippets with stops and some tags.')
    parser_josm.add_argument(
        'mode', metavar='<mode>', choices=osm.stoptags.keys(),
        help="Transit mode: 'train', 'subway', 'tram', 'bus' or 'ferry'.")
    parser_josm.add_argument(
        'type', metavar='<type>', choices=['route', 'stop'],
        help="Object type ('route', or 'stop')")
    parser_josm.add_argument(
        'refs', metavar='<ref>', nargs='+',
        help="One or more object references (e.g. '572', 'H1234')")
    parser_josm.set_defaults(func=sub_josm)

    parser_collect = subparsers.add_parser(
        'collect',
        help='Collect info from APIs for stops, stations, or routes for mode')
    parser_collect.add_argument(
        'mode', nargs='?', metavar='<mode> | stops | stations | citybikes',
        default="bus",
        help="stops, stations, citybikes or route mode: {} (default bus)"
        .format(", ".join(osm.stoptags.keys())))
    parser_collect.add_argument(
        '--interval-tags', '-i', action='store_true', dest='interval_tags',
        help="Collect info for 'interval*' tags for routes")
    parser_collect.add_argument(
        '--output', '-o', metavar='<output-file>', dest='output', default=None,
        help="Output to a file (default '<provider>_<mode_or_stops>.pickle')")
    parser_collect.set_defaults(func=sub_collect)

    parser_routes = subparsers.add_parser(
        'routes',
        help="Output a mediawiki report on routes from previously collected "
             "data in pickle format.")
    # TODO: Add start/stop limits by route ref
    parser_routes.add_argument(
        'file', metavar='<pickle-file>', help='Input file')
    parser_routes.add_argument(
        '--interval-tags', '-n', action='store_true', dest='interval_tags',
        help='Also report on "interval*" tags')
    parser_routes.add_argument(
        '--output', '-o', metavar='<output-file>', dest='output',
        default='-', help='Direct output to file (default stdout)')
    parser_routes.set_defaults(func=sub_routes)

    parser_stops = subparsers.add_parser(
        'stops',
        help="Output a mediawiki report on stops from previously collected "
             "data in pickle format, possibly limited by mode and/or city")
    parser_stops.add_argument(
        '--input', '-i', metavar='<input-file>', dest='input', default=None,
        help="Read from a pickle file (default '<provider>_stops.pickle')")
    parser_stops.add_argument(
        '--output', '-o', metavar='<output-file>', dest='output', default='-',
        help='Direct output to file (default stdout)')
#    parser_stops.add_argument('--format', '-f', metavar='<format>',
#        dest='format', default='mediawiki',
#        help='Output format: mediawiki (default), pickle')
    parser_stops.add_argument(
        'filt1', nargs='?', metavar='<mode>',
        help='Only report on stops with given mode: {}'.format(
            ", ".join(osm.stoptags.keys())))
    parser_stops.add_argument(
        'filt2', nargs='?', metavar='<city>',
        help='Only report on stops in given city: {}'.format(
            ", ".join(hsl.city2prefixes.keys())))
    parser_stops.set_defaults(func=sub_stops)

    parser_stations = subparsers.add_parser(
        'stations',
        help="Output a mediawiki report on stations from previously collected "
             "data in pickle format, possibly limited by mode")
    parser_stations.add_argument(
        '--input', '-i', metavar='<input-file>', dest='input', default=None,
        help="Read from a pickle file (default '<provider>_stations.pickle')")
    parser_stations.add_argument(
        '--output', '-o', metavar='<output-file>', dest='output', default='-',
        help='Direct output to file (default stdout)')
    parser_stations.add_argument(
        'filt1', nargs='?', metavar='<mode>',
        help='Only report on stations with given mode: {}'.format(
            ", ".join(osm.stoptags.keys())))
#    parser_stations.add_argument('filt2', nargs='?', metavar='<city>',
#        help='Only report on stations in given city: {}'\
#          .format(", ".join(hsl.city2prefixes.keys())))
    parser_stations.set_defaults(func=sub_stations)

    parser_citybikes = subparsers.add_parser(
        'citybikes',
        help="Output a mediawiki report on citybike stations from previously "
             "collected data in pickle format")
    parser_citybikes.add_argument(
        '--input', '-i', metavar='<input-file>', dest='input', default=None,
        help="Read from a pickle file (default '<provider>_citybikes.pickle')")
    parser_citybikes.add_argument(
        '--output', '-o', metavar='<output-file>', dest='output', default='-',
        help='Direct output to file (default stdout)')
    parser_citybikes.set_defaults(func=sub_citybikes)

#    parser_fullreport = subparsers.add_parser('fullreport',
#        help='Create a report for all lines.')
#    parser_fullreport.set_defaults(func=sub_fullreport)

    parser_help = subparsers.add_parser(
        'help', help="Show help for a subcommand")
    parser_help.add_argument(
        'subcmd', metavar='<subcommand>', nargs='?', default='taival',
        help="Print help for this subcommand.")

    def helpfun(arx):
        if arx.subcmd == 'taival':
            parser.print_help()
        else:
            subparsers.choices[args.subcmd].print_help()

    parser_help.set_defaults(func=helpfun)

    args = parser.parse_args()
    apikey = os.environ.get("DIGITRANSIT_API_KEY", None)
    pvd = digitransit.Digitransit(
        "https://api.digitransit.fi/routing/v2/hsl/gtfs/v1", "HSL",
        hsl.url, hsl.modecolors, hsl.peakhours, hsl.nighthours, hsl.shapetols, apikey)
    args.func(args)


if __name__ == '__main__':
    main()
