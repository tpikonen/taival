# Copyright (C) 2018-2021 Teemu Ikonen <tpikonen@gmail.com>

# This file is part of Taival.
# Taival is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License version 3 as published by the
# Free Software Foundation. See the file COPYING for license text.

import difflib
import re
import logging

import hsl
import osm

from routematching import (
    match_shapes_by_endpoints,
    match_stopcount,
)

from util import (
    citybikesortkey,
    get_multivalue_tag,
    haversine,
    linesortkey,
    test_shape_overlap,
)

from digitransit import citybike2url, pattern2url, terminalid2url

log = logging.getLogger(__name__)

outfile = None

style_problem = "background-color: #ffaaaa"
style_ok = "background-color: #aaffaa"
style_maybe = "background-color: #eeee00"
style_relstart = "border-style: solid; border-width: 1px 1px 1px 3px"
style_details = "background-color: #ffffff"

# Match codes with last number '0'
zerocodepat = re.compile("^..*0[^0-9]*$")


def wr(*args, **kwargs):
    kwargs["file"] = outfile
    print(*args, **kwargs)


def wr_if(s, **kwargs):
    """Write s if it's not empty."""
    if s:
        wr(s, **kwargs)


def keykey(dkey, return_type=str):
    return lambda x: x.get(dkey, return_type())


def test_tag(tags, key, value=None):
    """Test if a tag has a given value or just exists, if value=None.

    Return a string describing a problem, or empty string if no problems.
    """
    if key not in tags.keys():
        out = f"Tag '''{key}''' not set"
        out += "." if value is None else f" (should be '{value}')."
        return out

    if value is None:
        return ""

    for val in get_multivalue_tag(tags, key):
        if val == value:
            return ""
    valstr = tags[key]
    return f"Tag '''{key}''' has value '{valstr}' (should be '{value}')."


def test_badtag(tags, key):
    """Report error if key exists in tags."""
    if key in tags:
        val = tags[key]
        return f"Probably '''mispelled''' tag '''{key}''' with value '{val}'."
    else:
        return ""


def test_hsl_routename(ts, lineref, longname, stopnames=True):
    """Do a special test for the route name-tag.

    Return an empty string if ok, a string describing the problem if not.

    If stopnames is True, correct name is lineref + list of important stops
    derived from longname, else just lineref (for e.g. Saaristoliikenne
    ferry routes).
    """
    tag = ts.get("name", "")
    if stopnames:
        # Reittiopas longName field sometimes has dangling hyphens, remove them.
        longname = longname.strip('-')
        stops = hsl.longname2stops(longname)
        stops_unabbreviated = [hsl.synonyms_dict.get(s, s) for s in stops]
        # Allow en dash or ' - ' as stopname separator
        separators = ('–', ' - ')
        ok_names = []
        for sep in separators:
            for stopnames in (stops, stops_unabbreviated):
                ok_names.append(f"{lineref} {sep.join(stopnames)}")
                ok_names.append(f"{lineref} {sep.join(reversed(stopnames))}")

        suggestion = f"(should be either '{ok_names[0]}' or '{ok_names[1]}')."
    else:
        suggestion = f"(should be '{lineref}')."

    if tag == "":
        return "Tag '''name''' not set " + suggestion
    if stopnames:
        if tag in ok_names:
            return ""
    else:
        if tag == lineref:
            return ""

    return f"Tag '''name''' has value '{tag}' " + suggestion


def test_stop_positions(rel, mode="bus"):
    wr("'''Stop positions:'''\n")
    stops = [mem.resolve(resolve_missing=True)
             for mem in rel.members if mem.role == "stop"]
    platforms = [mem.resolve(resolve_missing=True)
                 for mem in rel.members if mem.role == "platform"]
    # wr("Route [%s %s] '%s' " % (osm.relid2url(rel.id), rel.id, \
    #      rel.tags.get("name", "<no-name-tag>")))
    wr("%d stop_positions vs. %d platforms." % (len(stops), len(platforms)))
    wr("")
#    # FIXME: Output something sensible
#    for s in stops:
#        test_tag(s.tags, "ref")
#        test_tag(s.tags, "name")
#        test_tag(s.tags, "public_transport", "stop_position")
#        test_tag(s.tags, mode, "yes")
#    for p in platforms:
#        p_name = p.tags.get("name", "<platform-has-no-name>")
#        p_ref = p.tags.get("ref", "<platform-has-no-ref>")
#        refmatches = [s for s in stops \
#          if s.tags.get("ref", "") == p_ref]
#        namematches = [s for s in stops \
#          if s.tags.get("name", "") == p_name]
#        sout = " Platform %s %s:" % (p_ref, p_name)
#        linit = len(sout)
#        if len(refmatches) < 1:
#            sout += " No stop_position with matching ref!"
#        elif len(refmatches) > 1:
#            sout +=" More than one stop_position with matching ref!"
#        if len(namematches) < 1:
#            sout += " No stop_position with matching name!"
#        elif len(namematches) > 1:
#            sout += " More than one stop_position with matching name!"
#        if len(sout) > linit:
#            wr(sout)
    wr("")


def print_abstract(md):
    mode = md["mode"]
    wr("This is a comparison of OSM public transit route data with "
       f"[{md['agencyurl']} {md['agency']}] {mode} transit data "
       "(via [http://digitransit.fi digitransit.fi]) "
       "generated by a [https://codeberg.org/tpikonen/taival script].\n")
    if mode == "subway":
        wr("See also [http://osmz.ru/subways/finland.html osmz.ru subway validator].\n")


def print_box(title, content):
    wr('{| class="wikitable"')
    wr('!style="text-align:left;"|%s' % (title))
    if content:
        wr("|-")
        wr("|")
        wr(content)
    wr("|}")


def print_summary(md):
    osmdict = md["osmdict"]
    hsldict = md["hsldict"]
    refless = md["refless"]
    osmlines = set(osmdict)
    hsllines = set(hsldict)
    wr("= Summary of %s transit in %s region. =" % (md["mode"], md["agency"]))
    wr("'''%d lines in OSM.'''\n" % len(osmlines))
    wr("'''%d lines in %s.'''\n" % (len(hsllines), md["agency"]))

    osmextra = osmlines.difference(hsllines)
    if md["mode"] == "bus":
        hsl_locallines = set(md["hsl_localbus"])
        osmextra = list(osmextra.difference(hsl_locallines))
    else:
        osmextra = list(osmextra)
    osmextra.sort(key=linesortkey)
    print_box(
        "%d lines in OSM but not in %s" % (len(osmextra), md["agency"]),
        ",\n".join(["%s (%s)" % (x, ", ".join(
            ["[%s %d]" % (osmdict[x][z], z + 1)
             for z in range(len(osmdict[x]))])) for x in osmextra]))
    wr("")

    hslextra = list(hsllines.difference(osmlines))
    hslextra.sort(key=linesortkey)
    print_box(
        "%d lines in %s but not in OSM" % (len(hslextra), md["agency"]),
        ",\n".join(["[%s %s]" % (hsldict[x], x) for x in hslextra]))
    wr("")

    print_box(
        f"{len(refless)} lines without a 'ref' tag",
        ",\n".join(["[%s %s]" % (
            osm.relid2url(r.id), r.tags.get("name", str(r.id))) for r in refless]))
    wr("")

#    commons = list(hsllines.intersection(osmlines))
#    commons.sort(key=linesortkey)
#    wr("%d lines in both %s and OSM." % (len(commons), md["agency"]))
#    if commons:
#        wr(" %s" % ", ".join(["%s (%s)" % \
#          (x, ", ".join(["[%s %d]" % (osmdict[x][z], z+1) \
#            for z in range(len(osmdict[x]))])) for x in commons] ))
#    wr("")


def print_localbus(md):
    hsl_localbus = md["hsl_localbus"]
    hsl_locallines = set(hsl_localbus)
    osmdict = md["osmdict"]
    osmlines = set(osmdict)
    lbuses = list(hsl_locallines)
    lbuses.sort(key=linesortkey)
    wr("= Local bus lines =")

    print_box(
        "%d bus routes with GTFS type 704 (lähibussi) in %s" % (
            len(lbuses), md["agency"]),
        ",\n".join(["[%s %s]" % (hsl_localbus[x], x) for x in lbuses]))
    wr("")

    lcommons = list(hsl_locallines.intersection(osmlines))
    lcommons.sort(key=linesortkey)
    print_box(
        "%d normal bus routes in OSM with %s local bus route number" % (
            len(lcommons), md["agency"]),
        ",\n".join(["%s (%s)" % (x, ", ".join(
            ["[%s %d]" % (osmdict[x][z], z + 1)
             for z in range(len(osmdict[x]))])) for x in lcommons]))
    wr("")

    osm_minibusdict = md["osm_minibusdict"]
    osm_minibuslines = list(osm_minibusdict)
    osm_minibuslines.sort(key=linesortkey)
    print_box(
        "%d route=minibus routes in OSM" % (len(osm_minibuslines)),
        ",\n".join(["%s (%s)" % (x, ", ".join(
            ["[%s %d]" % (osm_minibusdict[x][z], z + 1)
             for z in range(len(osm_minibusdict[x]))])) for x in osm_minibuslines]))
    wr("")


def print_oldlines(md):
    mode = md["mode"]
    wasroutes = md["wasroutes"]
    wr("= Old lines =")
    waslines = list(wasroutes)
    waslines.sort(key=linesortkey)
    print_box(
        "%d routes with type 'was:route=%s'" % (len(wasroutes), mode),
        ",\n".join(["%s (%s)" % (x, ", ".join(["[%s %d]" % (
            wasroutes[x][z], z + 1) for z in range(len(wasroutes[x]))]))
            for x in waslines]))
    wr("")

    disroutes = md["disroutes"]
    dislines = list(disroutes)
    dislines.sort(key=linesortkey)
    print_box(
        "%d routes with type 'disused:route=%s'" % (len(disroutes), mode),
        ",\n".join(["%s (%s)" % (x, ", ".join(["[%s %d]" % (
            disroutes[x][z], z + 1) for z in range(len(disroutes[x]))]))
            for x in dislines]))
    wr("")


def cell_route_master(ld):
    """Return a (style, cell, details) tuple for route master cell.

    The style is the cell style, cell is contents of the 'Master' cell in
    line table. Possible problems are reported in details string.
    """
    lineref = ld["lineref"]
    mode = ld["mode"]
    rms = ld["osmroutemasters"]

    nr = len(rms)
    if nr < 1:
        return "", "No", ""
    elif nr > 1:
        style = style_problem
        cell = "more than 1"
        details = (
            "'''Route master'''\n"
            "More than one route_master relations found: %s\n" % (
                ", ".join("[%s %s]" % (osm.relid2url(r), r) for r in rms.keys())))
        return style, cell, details

    # nr == 1
    rmid = list(rms.keys())[0]
    member_ids = rms[rmid]["members"]
    route_ids = list(ld["osmroutes"].keys())
    cell = "[%s %s]" % (osm.relid2url(rmid), rmid)
    detlist = []
    members_not_in_routes = [r for r in member_ids if r not in route_ids]
    if members_not_in_routes:
        detlist.append("route_master has extra members: %s" % (
            ", ".join("[%s %s]" % (osm.relid2url(r), r)
                      for r in members_not_in_routes)))
    routes_not_in_members = [r for r in route_ids if r not in member_ids]
    if routes_not_in_members:
        detlist.append("Found matching routes not in route_master: %s" % (
            ", ".join("[%s %s]" % (osm.relid2url(r), r)
                      for r in routes_not_in_members)))

    tags = rms[rmid]["tags"]
    detlist.append(test_tag(tags, "route_master", mode))
    detlist.append(test_tag(tags, "ref", lineref))
    detlist.append(test_tag(tags, "name"))
    detlist.append(test_tag(tags, "network", "HSL"))

    if any(detlist):  # prepend header
        style = style_problem
        cell = cell + "[[#{} | diffs]]".format(ld["lineref"])
        details = ("'''Route master:'''\n\n"
                   + "\n\n".join(s for s in detlist if s) + "\n\n")
    else:
        style = style_ok
        details = ""

    return style, cell, details


def print_routetable(md, linerefs=None, networkname=None, platformidx=2):
    """Print a route table and details on differences from modedict.

    Uses refs given in linerefs arg. The default None means use all refs.

    If the name of the network differs from agency/provider name, it can
    be given in the networkname arg.

    The platformidx arg gives the index of the member in the platform
    tuple (lat, lon, ref, name) to be compared in the platform sequence
    diffs. The default is 2=ref, but some routes do not have platforms with
    refs, so 3=name can also be used).
    """
    if not linerefs and linerefs is not None:
        return
    header = """{| class="wikitable"
|-
! style="border-style: none" |
! style="border-style: none" |
! style="border-style: none" |
! style="border-style: solid; border-width: 1px 1px 1px 3px" colspan=5 | Direction 0
! style="border-style: solid; border-width: 1px 1px 1px 3px" colspan=5 | Direction 1"""
    subheader = """|-
! Route
! Master
! Match
! style="border-style: solid; border-width: 1px 1px 1px 3px" | OSM
! {}
! Tags
! Shape
! Platf.
! style="border-style: solid; border-width: 1px 1px 1px 3px" | OSM
! {}
! Tags
! Shape
! Platf."""
    subheader = subheader.format(md["agency"], md["agency"])
    footer = "|}"

    def print_cells(cells, linecounter, statcounter, lines_w_probs):
        if (zerocodepat.match(line) and linecounter > 9) or linecounter > 30:
            wr(subheader)
            linecounter = 0
        linecounter += 1
        statcounter += 1
        wr("|-")
        if any(c[0] == style_problem for c in cells):
            cells[0] = (style_problem, "[[#{} | {}]]".format(line, line))
            lines_w_probs += 1
        else:
            cells[0] = (style_ok, str(line))
        for style, content in cells:
            wr('| style="{}" | {}'.format(style, content))
        return (linecounter, statcounter, lines_w_probs)

    mode = md["mode"]
    if not networkname:
        networkname = md["agency"]
    wr(f"= {networkname} {mode} lines in OSM =\n")
    wr(f"This table compares {networkname} {mode} routes with OSM routes.")
    wr("The checker uses [[Proposed_features/Refined_Public_Transport | "
       "Refined public transport schema]]")
    wr("as a reference.")
    wr("")
    wr(header)
    wr(subheader)
    linecounter = 0
    statcounter = 0
    lines_w_probs = 0

    if linerefs is None:
        linerefs = [e['lineref'] for e in md["lines"].values()]

    for line in linerefs:
        cells = []
        ld = md["lines"][line]
        if "osmroutes" not in ld.keys():
            continue
        ld["details"] = ""

        # Line number placeholder, edited later
        cells.append(("", ""))

        # Master
        rm_style, rm_cell, rm_details = cell_route_master(ld)
        if rm_details:
            ld["details"] += rm_details
        cells.append((rm_style, rm_cell))

        # Match

        # OSM route <-> provider pattern mapping
        # Get candidate pattern codes:

        log.debug(f"# Matching route {line}")
        osmroutes = ld["osmroutes"]
        pvdpatterns = ld.get("pvdpatterns", {})
        # Get candidates by matching stopcounts:
#        osm_stopcounts = sorted(
#            len([p for p in rt["platforms_and_stops"] if p[4].startswith("platform")])
#            for rt in osmroutes.values())
        osm_stopcounts = []
        for rid, rt in osmroutes.items():
            stops = len([p for p in rt["platforms_and_stops"]
                         if p[4].startswith("platform")])
            log.debug(f"{rid} has {stops} stops")
            osm_stopcounts.append(stops)
        osm_stopcounts.sort()

        nstops = osm_stopcounts[int(len(osm_stopcounts) / 2)]  # Median stops per route

        log.debug(f"Matching routes with match_stopcount, {nstops=}")
        codes = (
            match_stopcount(
                [(c, p) for c, p in pvdpatterns.items()
                 if p["directionId"] == 0], nstops)
            + match_stopcount(
                [(c, p) for c, p in pvdpatterns.items()
                 if p["directionId"] == 1], nstops))

#        # Other ways to generate candidate pattern codes
#
#        # All codes
#        codes = list(pvdpatterns.keys())
#
#        codes = pvd.codes_after_date(
#            lineref, datetime.date.today().strftime("%Y%m%d"), mode)
#
#        codes = pvd.codes_longest_per_direction(lineref, mode)
#        log.debug("Calling codes_longest_after_date")
#        codes = pvd.codes_longest_after_date(lineref, \
#                    datetime.date.today().strftime("%Y%m%d"), mode)
#
#        # Use just the ':01' route variant
#        cfilt = [c for c in codes if (len(c) - c.rfind(":01")) == 3]
#        if len(cfilt) >= len(rels):
#            codes = cfilt
#            log.debug("Using just first route variants: %s\n" % (str(codes)))

        log.debug("Matching route shapes with these pattern codes:")
        for c in codes:
            log.debug("[%s %s]" % (pattern2url(c), c))

        osmshapes = [r["geometry"] for r in osmroutes.values()]
        hslshapes = [pvdpatterns[c]["geometry"] for c in codes]
        (osm2hsl, hsl2osm) = match_shapes_by_endpoints(osmshapes, hslshapes)
        id2hslindex = {}
        relid2code = {}
        relids = list(osmroutes.keys())
        for i in range(len(relids)):
            id2hslindex[relids[i]] = osm2hsl[i]
            relid2code[relids[i]] = (codes[osm2hsl[i]] if osm2hsl[i] is not None
                                     else None)
        log.debug("Mapping:")
        for i in range(len(codes)):
            log.debug(" %s -> %s" % (
                codes[i], "None" if hsl2osm[i] is None else relids[hsl2osm[i]]))
        log.debug("")

        if len(osmroutes) == len(codes) and all(x is not None for x in osm2hsl):
            cells.append((style_ok, "Uniq."))
        elif len(osmroutes) > 2:
            ld["details"] += "More than 2 matching OSM routes found: %s.\n" % (
                ", ".join("[%s %d]" % (osm.relid2url(rid), rid)
                          for rid in osmroutes.keys()))
            cells.append((style_problem, "[[#{} | no]]".format(line)))
            (linecounter, statcounter, lines_w_probs) = print_cells(
                cells, linecounter, statcounter, lines_w_probs)
            wr('| colspan=10 | Matching problem, see details')
            continue
        elif len(codes) != 2:
            ld["details"] += (f"{len(codes)} route pattern(s) in "
                              f"{networkname}, matching may be wrong.\n")
            for i in range(len(relids)):
                ld["details"] += " %s -> %s\n" % (
                    relids[i], "None" if osm2hsl[i] is None else codes[osm2hsl[i]])
            for i in range(len(codes)):
                ld["details"] += " %s -> %s\n" % (
                    codes[i], "None" if hsl2osm[i] is None else relids[hsl2osm[i]])
            cells.append((style_maybe, "[[#{} | maybe]]".format(line)))
        else:
            cells.append((style_ok, "Uniq."))

        # Directions / OSM relations
        pvdtags = ld["pvdtags"]
        dirindex = 0
        for relid, route in osmroutes.items():  # always <= 2 routes at this point
            dirdetails = ""

            # OSM
            cells.append((style_relstart, "[%s %s]" % (osm.relid2url(relid), relid)))

            # HSL
            hsli = id2hslindex[relid]
            if hsli is not None:
                cells.append(("", "[%s %s]" % (pattern2url(codes[hsli]), codes[hsli])))
            else:
                cells.append(("", "N/A"))

            # Tags
            tdetlist = []
            osmtags = route["tags"]
            # name-tag gets a special treatment
            if networkname != 'Saaristoliikenne':
                tdetlist.append(test_hsl_routename(
                    osmtags, pvdtags["shortName"], pvdtags["longName"],
                    stopnames=True))
            else:
                tdetlist.append(test_hsl_routename(
                    osmtags, pvdtags["longName"], pvdtags["longName"],
                    stopnames=False))
            tdetlist.append(test_tag(osmtags, "network", networkname))
            tdetlist.append(test_tag(osmtags, "from"))
            tdetlist.append(test_tag(osmtags, "to"))
#            if md["modecolors"][mode]:
#                tdetlist.append(test_tag(osmtags, "colour", md["modecolors"][mode]))
            tdetlist.append(test_badtag(osmtags, "color"))
            if hsli is not None and md["interval_tags"] and "interval_tags" in ld:
                itags = ld["interval_tags"][codes[hsli]]
                for k in sorted(itags.keys()):
                    tdetlist.append(test_tag(osmtags, k, itags[k]))

            if any(tdetlist):
                dirdetails += ("'''Tags:'''\n\n"
                               + "\n\n".join([s for s in tdetlist if s]) + "\n\n")
                cells.append((style_problem, "[[#{} | diffs]]".format(line)))
            else:
                cells.append((style_ok, "OK"))
            ptv1 = any(r in ('forward', 'backward') for r in route['member_roles'])

            # Shape
            sdetlist = []
            if hsli is not None:
                # FIXME: Fix to use the value from pvdpatterns directly
                # lat, lon, ref, name
                hslplatforms = [(s['lat'], s['lon'], s['code'], s['name'])
                                for s in pvdpatterns[codes[hsli]]["stops"]]
                if ptv1:
                    sdetlist.append(
                        "Route [%s %s] " % (osm.relid2url(relid), relid)
                        + "has ways with 'forward' and 'backward' roles (PTv1).")
                    cells.append((style_problem, "PTv1"))
                elif len(hslshapes[hsli]) <= len(hslplatforms):
                    cells.append((style_maybe, "N/A"))
                else:
                    tol = md["shapetol"]
                    shape = route['geometry']
                    gaps = route['gaps']
                    ovl = test_shape_overlap(shape, hslshapes[hsli], tol=tol)
                    if gaps:
                        sdetlist.append("Route has '''gaps'''!")
                        sdetlist.append(
                            f"Route [{osm.relid2url(relid)} {relid}] overlap "
                            f"(tolerance {tol:.0f} m) with {networkname} pattern "
                            f"[{pattern2url(codes[hsli])} {codes[hsli]}] "
                            + "is '''%1.0f %%'''." % (ovl * 100.0))
                        cells.append((style_problem, "[[#{} | gaps]]".format(line)))
                    elif ovl <= 0.90:
                        sdetlist.append(
                            f"Route [{osm.relid2url(relid)} {relid}] overlap "
                            f"(tolerance {tol:.0f} m) with {networkname} pattern "
                            f"[{pattern2url(codes[hsli])} {codes[hsli]}] "
                            + "is '''%1.0f %%'''." % (ovl * 100.0))
                        cells.append((style_problem, "%1.0f%%" % (ovl * 100.0)))
                    elif ovl <= 0.95:
                        cells.append((style_maybe, "%1.0f%%" % (ovl * 100.0)))
                    else:
                        cells.append((style_ok, "%1.0f%%" % (ovl * 100.0)))
            else:
                sdetlist.append(
                    f"Shape for route {relid} not available from provider.\n")
                cells.append((style_problem, "[[#{} | N/A]]".format(line)))
            if any(sdetlist):
                dirdetails += "'''Shape:'''\n\n" + "\n\n".join(sdetlist) + "\n\n"

            # Platforms
            hsli = id2hslindex[relid]
            if hsli is not None:
                no_platforms = not any(p[4].startswith('platform')
                                       for p in route["platforms_and_stops"])
                osmplatforms = [p for p in route["platforms_and_stops"]
                                if p[4].startswith("platform")]
                # FIXME: Add stop names to unified diffs after diffing, somehow
                if networkname == 'HSL' and platformidx == 2:
                    osmp = [re.sub(r"^([0-9]{4,4})$", r"H\1", p[2]) + "\n"
                            for p in osmplatforms]
                    hslp = [re.sub(r"^([0-9]{4,4})$", r"H\1", p[2]) + "\n"
                            for p in hslplatforms]
                else:
                    osmp = [p[platformidx] + "\n" for p in osmplatforms]
                    hslp = [p[platformidx] + "\n" for p in hslplatforms]
                diff = list(difflib.unified_diff(osmp, hslp, "OSM", networkname))
                if not osmp or diff or no_platforms:
                    dirdetails += "'''Platforms:'''\n\n"
                if no_platforms:
                    dirdetails += ("This route has platforms marked with role 'stop', "
                                   "role 'platform' is recommended.\n\n")
                if not osmp:
                    dirdetails += (f"{len(osmp)}/{len(hslp)} platforms in "
                                   f"OSM / {networkname}.\n\n")
                    cells.append((style_problem,
                                  f"[[#{line} | {len(osmp)}/{len(hslp)}]]"))
                elif diff:
                    dirdetails += (f"{len(osmp)}/{len(hslp)} platforms in "
                                   f"OSM / {networkname}.\n")
                    dirdetails += " " + diff[0]
                    dirdetails += " " + diff[1]
                    ins = 0
                    rem = 0
                    for d in diff[2:]:
                        dirdetails += " " + d
                        if d[0] == '+':
                            ins += 1
                        elif d[0] == '-':
                            rem += 1
                    cells.append((style_problem, "[[#{} | +{} -{}{}]]".format(
                                  line, ins, rem, "(s)" if no_platforms else "")))
                elif no_platforms:
                    cells.append((style_maybe, "[[#{} | {}/{}(s)]]".format(
                                  line, len(osmp), len(hslp))))
                else:
                    cells.append((style_ok, "{}/{}".format(len(osmp), len(hslp))))
            else:
                dirdetails += "'''Platforms:'''\n\n"
                dirdetails += "Platforms could not be compared.\n\n"
                cells.append((style_problem, "[[#{} | N/A]]".format(line)))

            # Add per direction details
            if dirdetails:
                pvd_link = (f"[{pattern2url(codes[hsli])} {codes[hsli]}]"
                            if hsli is not None else '<No HSL route>')
                ld["details"] += (
                    f"'''Direction {dirindex}''', "
                    f"route [{osm.relid2url(relid)} {relid}], {pvd_link}\n\n"
                    + dirdetails)
            dirindex += 1
        # end 'for relid, route in osmroutes.items()'

        (linecounter, statcounter, lines_w_probs) = print_cells(
            cells, linecounter, statcounter, lines_w_probs)

    wr(footer)
    wr("")
    wr(f"{statcounter} lines total.\n")
    wr(f"{lines_w_probs} lines with differences.\n")

    # details
    if any(ld.get("details", None) for ld in md["lines"].values()):
        pass
        # No separate subheader for differences any more
        # wr("= Details on differences =\n")
    else:
        return
    for ld in [md["lines"][ref] for ref in linerefs]:
        if ld.get("details", None):
            wr("== {} ==".format(ld["lineref"]))
            wr(ld["details"])


def report_routes(md):
    """Write a mediawiki report page on routes with summary and line table."""
    print_abstract(md)
    print_summary(md)
    if md["mode"] == "bus":
        print_localbus(md)
    print_oldlines(md)
    if md["agency"] == "HSL" and md["mode"] == "ferry":
        linerefs = [e['lineref'] for e in md["lines"].values() if "pvdtags" in e.keys()
                    and not e["pvdtags"]["gtfsId"].startswith('HSLlautta')]
        print_routetable(md, linerefs)
        lautat = [e['lineref'] for e in md["lines"].values() if "pvdtags" in e.keys()
                  and e["pvdtags"]["gtfsId"].startswith('HSLlautta')]
        print_routetable(md, lautat, "Saaristoliikenne", 3)
    else:
        print_routetable(md)


def check_mode(os, ps):
    """Return a (style, text, details) tuple on mode related OSM tags."""
    modelist = osm.stoptags2mode(os)
    pmode = ps["mode"]
    if not modelist:
        details = f"No mode found, provider has mode '{pmode}'."
        return (style_problem, pmode, details)
    elif len(modelist) > 1:
        # FIXME: This is a hack to get around the fact that digitransit API
        # does not have a light_rail mode.
        # Treat light_rail as tram.
        if pmode in modelist or (pmode == 'tram' and 'light_rail' in modelist):
            m = "+".join(modelist)
            return (style_maybe, m, "")
        else:
            details = "OSM tags match modes: {}. provider has mode '{}'.".format(
                ", ".join(modelist), pmode)
            return (style_problem, pmode, details)
    else:
        omode = modelist[0]
        # FIXME: light_rail == tram hack
        if omode == pmode or (pmode == 'tram' and omode == 'light_rail'):
            return (style_ok, omode, "")
        else:
            details = f"Mode from OSM tags is '{omode}', provider has mode '{pmode}'."
            return (style_problem, pmode, details)


def check_type(os, ps):
    type2style = {
        "node": style_ok,
        "way": style_maybe,
        "relation": style_problem,
    }
    type2style_railstop = {
        "node": style_maybe,
        "way": style_ok,
        "relation": style_maybe,
    }
    osmtype = osm.xtype2osm[os["x:type"]]
    style = (type2style_railstop[osmtype]
             if (ps.get('mode', '') in ('train', 'subway')
                 and os.get('railway', '') != 'station')
             else type2style[osmtype])
    text = "[{} {}]".format(osm.obj2url(os), osmtype)
    return style, text


def check_dist(os, ps):
    """Return a (style, text) tuple for distance between OSM and provider stops."""
    op = os.get("x:latlon", None)
    pp = ps.get("latlon", None)
    if op and pp:
        dist = haversine(op, pp)
        if dist > 0.999:
            return style_problem, "{0:.1f} km".format(dist)
        elif dist > 0.050:
            return style_problem, "{0:.0f} m".format(dist * 1000)
        else:
            return style_ok, "{0:.0f} m".format(dist * 1000)
    else:
        return style_problem, "Error"


def shorten(s, maxlen=25):
    return s if len(s) < maxlen else s[:(maxlen - 5)] + u"\u2026" + s[-3:]


def check_name(os, ps):
    """Return (style, text, details) cell tuple comparing name value."""
    on = os.get("name")
    pn = ps.get("name")
    cn = pn if pn else "<no name in data>"
    cn = shorten(cn)

    if not on:
        details = "'''name''' not set in OSM, provider has '{}'.".format(pn)
        return (style_problem, cn, details)
    elif on == pn:
        namefi = os.get("name:fi")
        namesv = os.get("name:sv")
        if namefi and namesv and namefi != on and namesv != on:
            details = (f"'''name:fi'''={namefi} and '''name:sv'''={namesv}, "
                       f"but '''name'''={on}.")
            return (style_problem, cn, details)
        else:
            return (style_ok, cn, "")
    elif len(on) > 2 and on[-1] in 'PIEL' and on[-2] == ' ':
        # Allow direction suffixes like 'Susiraja I'
        stem = on[:-2]
        if stem == pn:
            return (style_ok, cn, "")
        else:
            details = "'''name''' set to '{}', should be '{}'.".format(on, pn)
            return (style_problem, cn, details)
    else:
        # Allow platform suffix like 'Susiraja, laituri 3'
        platform = hsl.get_platform(ps)
        platform_fmt = "{}, laituri {}"
        platsuffix = on.find(", laituri")
        stem = on[:platsuffix] if platform and platsuffix > 1 else on
        stem_ok = (stem == pn
                   or any(stem == pn.replace(abbr, repl)
                          for abbr, repl in hsl.synonyms))

        if not stem_ok:
            details = "'''name''' set to '{}', should be '{}'.".format(on, pn)
            return (style_problem, cn, details)
        elif platsuffix > 1:
            if on == platform_fmt.format(stem, platform):
                return (style_ok, shorten(on), "")
            else:
                details = "'''name''' set to '{}', should be '{}'.".format(on, pn)
                return (style_problem, cn, details)
        else:
            return (style_ok, shorten(on), "")


def check_findr(os, ps):
    """Return (style, text, details) tuple for ref:findr tag in OSM.

    Compares value to Digiroad data.
    """
    # FIXME: Should also compare by position, useless as it is now
    findr = os.get("ref:findr", None)
    import digiroad
    dlist = digiroad.stops_by_name.get(ps["name"], [])
    drid = None
    mindist = 1e6
    olatlon = os["x:latlon"]
    for d in dlist:
        dist = haversine(olatlon, (float(d['stop_lat']), float(d['stop_lon'])))
        if dist < mindist:
            mindist = dist
            drid = d["stop_id"]
    if findr:
        if drid:
            if findr == drid:
                return (style_ok, str(findr), "")
            else:
                details = f"'''ref:findr''' is '{findr}', should be '{d}'."
                return (style_problem, "diff", details)
        else:
            details = "'''ref:findr''' is set, but not found from Digiroad."
            return (style_maybe, "{}/-".format(findr), details)
    else:
        if drid:
            return (style_problem, str(drid), "")
        else:
            # details = f"Digiroad ID for stop name '{ps['name']}' missing."
            details = ""
            return (style_problem, "N/A", details)


def check_local_ref(os, ps):
    """Return (style, text) cell tuple comparing local_ref value."""
    pp = hsl.get_platform(ps)
    op = os.get("local_ref")
    if op:
        if pp is None:
            return (style_maybe, f"{op}/-")
        elif op == pp:
            return (style_ok, op)
        else:
            return (style_problem, f"{op}/{pp}")
    else:
        if pp is None:
            return (style_ok, "-")
        else:
            return (style_problem, "-/{}".format(pp))


def check_zone(os, ps):
    """Return (style, text) cell tuple comparing zone:HSL value."""
    oz = os.get("zone:HSL", None)
    pz = ps.get("zoneId", "?")
    pz = "no" if pz == "Ei HSL" else pz
    if oz:
        if oz == pz:
            return (style_ok, oz)
        else:
            return (style_problem, "{}/{}".format(oz, pz))
    else:
        if pz == 'no':
            return (style_ok, "-/-")
        else:
            return (style_problem, "-/{}".format(pz))


def check_wheelchair(os, ps):
    """Return (cell style, text) tuple comparing wheelchair accessibility for a stop."""
    ow = os.get("wheelchair", None)
    pw = ps.get("wheelchairBoarding", None)
    if not ow:
        if not pw:
            return (style_problem, "-/err1")
        elif pw == "NO_INFORMATION":
            return (style_ok, "-/?")
        elif pw == "POSSIBLE":
            return (style_problem, "-/yes")
        elif pw == "NOT_POSSIBLE":
            return (style_problem, "-/no")
        else:
            return (style_problem, "-/err2")
    elif ow == 'yes' or ow == 'limited' or ow == 'designated':
        if not pw:
            return (style_problem, "yes/err1")
        elif pw == "NO_INFORMATION":
            return (style_maybe, "yes/?")
        elif pw == "POSSIBLE":
            return (style_ok, "yes/yes")
        elif pw == "NOT_POSSIBLE":
            return (style_problem, "yes/no")
        else:
            return (style_problem, "yes/err2")
    elif ow == 'no':
        if not pw:
            return (style_problem, "no/err1")
        elif pw == "NO_INFORMATION":
            return (style_maybe, "no/?")
        elif pw == "POSSIBLE":
            return (style_problem, "no/yes")
        elif pw == "NOT_POSSIBLE":
            return (style_ok, "no/no")
        else:
            return (style_problem, "no/err2")
    else:
        return (style_problem, "err")


def print_stopline(oslist, oldstops, ps, cols):
    """Print a line to stop table, return (nlines, isok)."""
    ref = ps["code"]
    # Prefer exact ref matches (i.e. Hnnnn), if there are none, use all
    flist = [e for e in oslist if e.get("ref", None) == ref]
    oslist = flist if flist else oslist
    linecounter = 1
    detlist = []
    wr("|-")
    wr("| [https://reittiopas.hsl.fi/pysakit/{} {}]".format(ps["gtfsId"], ref))
    isok = True
    if len(oslist) == 1:
        def wr_cell(f, o, p):
            tt = f(o, p)
            st = tt[0]
            txt = tt[1]
            if len(tt) > 2 and tt[2]:
                detlist.append(tt[2])
            wr('| style="{}" | {}'.format(st, txt))
            return st != style_problem

        os = oslist[0]
        isok &= wr_cell(check_name, os, ps)
        isok &= wr_cell(check_mode, os, ps)
        isok &= wr_cell(check_type, os, ps)
        isok &= wr_cell(check_dist, os, ps)
        isok &= wr_cell(check_local_ref, os, ps)
        isok &= wr_cell(check_zone, os, ps)
#        isok &= wr_cell(check_findr, os, ps)
        isok &= wr_cell(check_wheelchair, os, ps)
        if detlist:
            isok = False
            linecounter += len(detlist)
            wr('|-')
            wr('| colspan={} style="{}" | {}'.format(
                cols, style_details, "\n".join(detlist)))
    elif len(oslist) > 1:
        isok = False
        (lat, lon) = ps["latlon"]
        wr(f'| colspan={cols - 1} style="{style_problem}" '
           '| More than one stop with the same ref in OSM')
        wr('|-')
        desc = "\nMatching stops in OSM: {}.".format(", ".join(
            ["[https://www.openstreetmap.org/{}/{} {}]".format(
                osm.xtype2osm[e["x:type"]], e["x:id"], e["x:id"]) for e in oslist]))
        wr('| colspan={} style="{}" | {}'.format(cols, style_details, desc))
        linecounter += 1
    else:
        isok = False
        lat, lon = ps["latlon"]
        wr(f'| colspan={cols - 1} style="{style_problem}" '
           f'| {ps["name"]} is missing from '
           f'[https://www.openstreetmap.org/#map=19/{lat}/{lon} OSM]')
        wr('|-')

        if oldstops is not None and ref in oldstops.keys():
            oldlist = oldstops[ref]
            desc = "Old stops in OSM: {}.".format(", ".join(
                ["[https://www.openstreetmap.org/{}/{} {}]".format(
                    osm.xtype2osm[e["x:type"]], e["x:id"], e["x:id"])
                    for e in oldlist]))
            wr(f'| colspan={cols} style="{style_details}" | {desc}')
            linecounter += 1
        else:
            taglist = []
            taglist.append("'''name'''='{}'".format(ps.get("name")))
            pp = hsl.get_platform(ps)
            if pp is not None:
                taglist.append(f"'''local_ref'''='{pp}'")
            pz = ps.get("zoneId", None)
            if pz and pz != "Ei HSL":
                taglist.append(f"'''zone:HSL'''='{pz}'")
            pw = ps.get("wheelchairBoarding", None)
            if pw and pw == 'POSSIBLE':
                taglist.append("'''wheelchair'''='yes'")
            elif pw and pw == 'NOT_POSSIBLE':
                taglist.append("'''wheelchair'''='no'")
            desc = "Mode is {}. ".format(ps["mode"])
            desc += "Tags from provider: " + ", ".join(taglist) + "."
            wr('| colspan={} style="{}" | {}'.format(cols, style_details, desc))
            linecounter += 1
    return linecounter, isok


def print_stoptable(sd, stops=None):
    """Print a stoptable for stops."""
    cols = 8
    header = '{| class="wikitable"'
    subheader = """|-
! ref
! name
! mode
! type
! delta
! local_ref
! zone:HSL
! wheelchair"""
    footer = "|}"

    ost = sd["ost"]
    oldstops = sd.get("oldstops")
    pst = sd["pst"]  # data from provider

    if stops is None:
        stops = [v for m in pst.keys() for v in pst[m].values()]
        stops.sort(key=keykey("code"))

    linecounter = 0
    stopcounter = 0
    probcounter = 0
    wr(header)
    wr(subheader)
    for ps in stops:
        ref = ps["code"]
        if (zerocodepat.match(ref) and linecounter > 9) or linecounter > 30:
            wr(subheader)
            linecounter = 0
        oslist = ost.get(ref, [])
        nlines, isok = print_stopline(oslist, oldstops, ps, cols)
        # linecounter += nlines
        linecounter += 1  # Makes diffs more stable
        stopcounter += 1
        probcounter += 0 if isok else 1
    wr(footer)
    wr("")
    wr("{} stops.\n".format(stopcounter))
    wr("{} stops with differences.\n".format(probcounter))


def report_stops(sd, mode=None, city=None):
    """Output a report on stops. Either all, or limited by mode, city or both."""
    header = ("= {} stops".format(sd["agency"]) if not mode
              else "= {} {} stops".format(sd["agency"], mode))
    header += " =\n" if not city else " in {} =\n".format(city)
    wr("__FORCETOC__")
    wr("This is a comparison of OSM public transit stop data with "
       "[https://www.hsl.fi/ HSL] data (via [http://digitransit.fi digitransit.fi]) "
       "generated by a [https://codeberg.org/tpikonen/taival script].\n")
    wr(header)

    ost = sd["ost"]
    pst = sd["pst"]

    if mode:
        stops = list(pst[mode].values())
        orefs = (r for r, vl in ost.items()
                 if mode in (m for v in vl for m in osm.stoptags2mode(v)))
        oldstops = [val for val in sd["oldstops"].values()
                    if mode in (m for v in val for m in osm.stoptags2mode(v))]
    else:
        stops = [v for m in pst.keys() for v in pst[m].values()]
        orefs = ost.keys()
        oldstops = list(sd["oldstops"].values())
    if city:
        prefixes = hsl.city2prefixes[city]
        # temporary(?) stops to be added to "OSM stops not in HSL" section
        prefixes.extend(["X" + p for p in prefixes])
        pattern = re.compile("^(" + "|".join(prefixes) + ")[0-9]{4,4}$")
        stops = [s for s in stops if pattern.match(s["code"])]
        orefs = (r for r in orefs if pattern.match(r))
        oldstops = [sl for sl in oldstops if any(pattern.match(s["ref"]) for s in sl)]
    stops.sort(key=keykey("code"))
    print_stoptable(sd, stops)

    # Old stops
    wr("= Old stops =\n")
    print_box(
        f"{len(oldstops)} stops with 'was:' or 'disused:' prefix and ref",
        ",\n".join(osm.stoplist2links(sl) for sl in oldstops))
    wr("")

    # Stops not in provider data
    wr("= OSM stops not in {} =\n".format(sd["agency"]))
    orefs = set(orefs)
    pstops = {ps["code"] for ps in stops}  # set comprehension
    extras = orefs.difference(pstops)
    hslpat = re.compile(r"^[^0-9]*[0-9]{4,4}$")
    extras = [r for r in extras if hslpat.match(r)]
    extras.sort()
    print_box(
        "{} stops not in {}".format(len(extras), sd["agency"]),
        ",\n".join(osm.stoplist2links(ost[ref]) for ref in extras))


def check_stationname(os, ps):
    """Return (style, text, details) cell tuple comparing station names."""
    # TODO: check that name:fi matches name
    on = os.get("name", None)
    pn = ps.get("name", None)
    complen = 4
    if on:
        if on == pn:
            return (style_ok, on, "")
        elif len(on) >= complen and pn.startswith(on[:complen]):
            return (style_maybe, on, "")
        else:
            return (style_problem, on, "")
    else:
        return (style_problem, "<no name in OSM>", "")


def print_stationline(os, ps, cols):
    def wr_cell(f, o, p):
        tt = f(o, p)
        st = tt[0]
        txt = tt[1]
        if len(tt) > 2 and tt[2]:
            detlist.append(tt[2])
        wr('| style="{}" | {}'.format(st, txt))
        return st != style_problem

    dist = haversine(os["x:latlon"], ps["latlon"]) if os is not None else 1e3
    linecounter = 1
    isok = True
    detlist = []
    wr("|-")
    wr("| [{} {}]".format(terminalid2url(ps["gtfsId"]), ps["name"]))
    if dist > 0.200:
        isok = False
        (lat, lon) = ps["latlon"]
        wr(f'| colspan={cols - 1} style="{style_problem}" | Station not found in '
           f'[https://www.openstreetmap.org/#map=19/{lat}/{lon} OSM]')
    else:
        os["x:matched"] = True
        isok &= wr_cell(check_stationname, os, ps)
        isok &= wr_cell(check_mode, os, ps)
        isok &= wr_cell(check_type, os, ps)
        wr("| {0:.0f} m".format(dist * 1000))
    if detlist:
        isok = False
        linecounter += len(detlist)
        wr('|-')
        wr('| colspan={} style="{}" | {}'.format(
            cols, style_details, "\n".join(detlist)))
    return linecounter, isok


def print_stationtable(sd, mode):
    """Print a table of 'mode' stations."""
    import numpy as np
    from pykdtree.kdtree import KDTree
    cols = 5
    header = '{| class="wikitable"'
    subheader = """|-
! HSL name
! OSM name
! mode
! type
! delta"""
    footer = "|}"

    ost = sd["ostat"]
    pst = sd["pstat"]  # data from provider

    stations = pst[mode]
    stations.sort(key=keykey('name'))
    ostats = ost[mode]
    kd_tree = KDTree(np.array([e["x:latlon"] for e in ostats])) if ostats else None

    linecounter = 0
    statcounter = 0
    probcounter = 0
    wr(header)
    wr(subheader)
    for ind in range(len(stations)):
        if linecounter > 19:
            wr(subheader)
            linecounter = 0
        ps = stations[ind]
        if kd_tree is not None:
            darr, iarr = kd_tree.query(np.array(ps["latlon"], ndmin=2))
            os = ostats[iarr[0]]
        else:
            os = None
        nlines, isok = print_stationline(os, ps, cols)
        # linecounter += nlines
        linecounter += 1  # Makes diffs more stable
        statcounter += 1
        probcounter += 0 if isok else 1
    wr(footer)
    wr("")
    wr("{} stations.\n".format(statcounter))
    wr("{} stations with differences.\n".format(probcounter))
    not_in = [s for s in ostats if "x:matched" not in s.keys()]
    if not_in:
        not_in.sort(key=keykey("name"))
        print_box(
            "{} stations not in HSL".format(mode.capitalize()),
            ",\n".join("[%s %s]" % (osm.obj2url(s), s.get("name", "<no name in OSM>"))
                       for s in not_in))


def report_stations(sd, mode=None):
    """Output a report on stations. Either all, or limited by mode."""
    pstat = sd["pstat"]
    wr("This is a comparison of OSM public transit station data with "
       "[https://www.hsl.fi/ HSL] data (via [http://digitransit.fi digitransit.fi]) "
       "generated by a [https://codeberg.org/tpikonen/taival script].\n")
    if mode:
        modelist = [mode]
    else:
        modelist = list(pstat.keys())
        wr("= Stations in HSL =\n")
    modelist.sort()
    for m in modelist:
        wr("== {} stations ==".format(m.capitalize()))
        print_stationtable(sd, m)


def check_cbname(os, ps):
    """Return (style, text, details) tuple comparing citybike station name value."""
    lang_extensions = ['', ':fi', ':en', ':sv']
    detlist = []
    isok = True
    for tagext in lang_extensions:
        on = os.get("name" + tagext, None)
        pn = ps.get("name" + tagext, None)
        pn = pn.replace('\xa0', ' ') if pn else pn
        if not pn:
            continue
        if on:
            if (on == pn or any(
                    pn.replace(rep, abb) == on for rep, abb in hsl.synonyms)):
                isok &= True
            else:
                detlist.append(f"'''name{tagext}''' set to '{on}', should be '{pn}'.")
                isok = False
        else:
            if tagext != ':fi':
                detlist.append(f"'''name{tagext}''' not set in OSM, should be '{pn}'.")
            if tagext == '':
                isok = False

    on = os.get("name", "")
    pn = ps["name"]
    if on == pn or any(pn.replace(repl, abbr) == on for repl, abbr in hsl.synonyms):
        goodname = on
    else:
        goodname = pn

    return (style_ok if isok else style_problem, goodname, "\n".join(detlist))


def check_capacity(os, ps):
    """Return (style, text, details) cell tuple comparing citybike station capacity."""
    oc = os.get("capacity", None)
    if ps["state"] != 'Station on':
        cell = "{} / N/A".format(oc if oc else "-")
        return (style_maybe, cell, "")
    pc = str(ps.get("capacity", "?"))
    if oc:
        if oc == pc:
            return (style_ok, pc, "")
        else:
            tol = 0.4
            cmin = int((1.0 - tol) * int(pc))
            cmax = int((1.0 + tol) * int(pc))
            cell = "{}/{}".format(oc, pc)
            ioc = int(oc)
            if ioc > cmin and ioc < cmax:
                return (style_maybe, cell, "")
            else:
                return (style_problem, cell, "")
    else:
        cell = "-/{}".format(pc)
        # details = "'''capacity''' not set in OSM, HSL has '{}'.".format(pc)
        return (style_problem, cell, "")


def check_cbnetwork(os, ps):
    """Return (style, text, details) cell tuple comparing citybike network tag."""
    on = os.get("network")
    pn = ps.get("network")

    if pn is None:
        if on is None:
            return (style_maybe, '-', "")
        else:
            return (style_maybe, on, "")

    n2n = {
        'Helsinki': 'Helsinki&Espoo',
        'Vantaa': 'Vantaa',
    }
    correct = n2n.get(pn, 'N/A')

    if on is None:
        details = "'''network''' not set, should be '{}'.".format(correct)
        return (style_problem, correct, details)

    if on == correct:
        return (style_ok, on, "")
    elif correct == 'N/A':
        return (style_maybe, on, "")
    else:
        details = "'''network'''='{}', should be '{}'.".format(on, correct)
        return (style_problem, correct, details)


def print_citybikeline(oslist, ps, cols):
    """Print a line to citybike table, return (nlines, isok)."""
    ref = ps["stationId"]
    linecounter = 1
    detlist = []
    wr("|-")
    wr("| [{} {}]".format(citybike2url(ref), ref))
    isok = True
    if len(oslist) == 1:
        def wr_cell(f, o, p):
            tt = f(o, p)
            st = tt[0]
            txt = tt[1]
            if len(tt) > 2 and tt[2]:
                detlist.append(tt[2])
            wr('| style="{}" | {}'.format(st, txt))
            return st != style_problem

        os = oslist[0]
        isok &= wr_cell(check_cbname, os, ps)
        isok &= wr_cell(check_type, os, ps)
        isok &= wr_cell(check_dist, os, ps)
        isok &= wr_cell(check_capacity, os, ps)
        isok &= wr_cell(check_cbnetwork, os, ps)
        if detlist:
            isok = False
            linecounter += len(detlist)
            wr('|-')
            wr('| colspan={} style="{}" | {}'.format(
                cols, style_details, "\n".join(detlist)))
    elif len(oslist) > 1:
        isok = False
        wr('| {}'.format(ps["name"]))
        (lat, lon) = ps["latlon"]
        wr(f'| colspan={cols - 2} style="{style_problem}" '
           '| More than one station with the same ref in OSM')
        wr('|-')
        desc = "\nMatching stations in OSM: {}.".format(
            ", ".join(["[https://www.openstreetmap.org/{}/{} {}]".format(
                       osm.xtype2osm[e["x:type"]], e["x:id"], e["x:id"])
                       for e in oslist]))
        wr('| colspan={} style="{}" | {}'.format(cols, style_details, desc))
        linecounter += 1
    else:
        isok = False
        wr('| {}'.format(ps["name"]))
        (lat, lon) = ps["latlon"]
        wr(f'| colspan={cols - 2} style="{style_problem}" '
           f'| missing from [https://www.openstreetmap.org/#map=19/{lat}/{lon} OSM]')
        wr('|-')
        taglist = []
        taglist.append("'''name'''='{}'".format(ps["name"]))
#        if ps["state"] == 'Station on':
#            cap = str(int(ps['bikesAvailable']) + int(ps['spacesAvailable']))
#            taglist.append("'''capacity'''='{}'".format(cap))
#        if "total_slots" in ps.keys():
#            taglist.append("'''capacity'''='{}'".format(ps["total_slots"]))
        desc = "Tags from HSL: " + ", ".join(taglist) + "."
        wr('| colspan={} style="{}" | {}'.format(cols, style_details, desc))
        linecounter += 1
    return linecounter, isok


def print_citybiketable(sd, refs=None):
    """Print a table of citybike stations."""
    cols = 6
    header = '{| class="wikitable"'
    subheader = """|-
! ref
! name
! type
! delta
! capacity
! network"""
    footer = "|}"

    ost = sd["osm_cbs"].copy()
    pst = sd["pvd_cbs"]  # data from provider

    refs = refs if refs is not None else list(pst.keys())
    refs.sort(key=citybikesortkey)

    if refs:
        wr(header)
        wr(subheader)

    linecounter = 0
    statcounter = 0
    probcounter = 0
    for ref in refs:
        if (zerocodepat.match(ref) and linecounter > 9) or linecounter > 30:
            wr(subheader)
            linecounter = 0
        ps = pst[ref]
        if not ps["networks"][0] in ["vantaa", "smoove"]:
            continue
        oslist = ost.pop(ref, [])
        nlines, isok = print_citybikeline(oslist, ps, cols)
        # linecounter += nlines
        linecounter += 1  # Makes diffs more stable
        statcounter += 1
        probcounter += 0 if isok else 1

    if refs:
        wr(footer)
        wr("")

    wr("{} citybike stations.\n".format(statcounter))
    wr("{} citybike stations with differences.\n".format(probcounter))


def print_old_citybikes(sd):
    old_cbs = sd.get('old_cbs')
    old_prefixes = sd.get('old_cbs_prefixes')

    if old_cbs is None:
        wr("No Old city bike stations found.\n")
        return

    out = []
    for ref in sorted(old_cbs.keys(), key=citybikesortkey):
        vallist = old_cbs[ref]
        if len(vallist) == 1:
            url = osm.obj2url(vallist[0])
            out.append(f"[{url} {ref}]")
        else:
            linklist = ", ".join("[%s %d]" % (
                osm.obj2url(obj), num) for num, obj in enumerate(vallist))
            out.append(f"{ref} ({linklist})")
    print_box(
        "%d Old city bike stations with lifecycle prefixes %s." % (
            len(old_cbs), ', '.join(old_prefixes)),
        ",\n".join(out))
    wr("")


def report_citybikes(sd):
    """Output a report on citybike stations."""
    ost = sd["osm_cbs"]
    pst = sd["pvd_cbs"]
    wr("__FORCETOC__")
    wr("This is a comparison of OSM citybike station data with "
       "[https://www.hsl.fi/ HSL] data (via [http://digitransit.fi digitransit.fi]) "
       "generated by a [https://codeberg.org/tpikonen/taival script].\n")
    # wr("= Citybike stations in HSL =\n")

    wr("== Active citybike stations ==\n")
    active_refs = [v["stationId"] for v in pst.values() if v["state"] == "Station on"]
    print_citybiketable(sd, active_refs)

    wr("== Citybike stations not in use ==\n")
    inactive_refs = [v["stationId"] for v in pst.values() if v["state"] != "Station on"]
    print_citybiketable(sd, inactive_refs)

    wr("== Old citybike stations ==\n")
    print_old_citybikes(sd)

    wr("== Other citybike stations ==\n")
    printed = set(active_refs + inactive_refs)
    rest_w_ref = [e for vals in ost.values() for e in vals if not e["ref"] in printed]
    rest_w_ref.sort(key=lambda x: citybikesortkey(keykey("ref")(x)))
    orest = list(sd["osm_cbs_rest"].values())
    orest.sort(key=keykey("name"))
    rlist = ["[{} {}]".format(
        osm.obj2url(s), s.get("ref", "<no ref in OSM>")) for s in rest_w_ref]
    olist = ["[{} {}]".format(
        osm.obj2url(s), s.get("name", "<no name in OSM>")) for s in orest]
    print_box(
        "{} citybike stations not in {}\n".format(
            len(rest_w_ref) + len(orest), sd["agency"]),
        ",\n".join(rlist + olist))
