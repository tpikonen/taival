# Copyright (C) 2018-2023 Teemu Ikonen <tpikonen@gmail.com>

# This file is part of Taival.
# Taival is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License version 3 as published by the
# Free Software Foundation. See the file COPYING for license text.

import re
from collections import defaultdict

url = "https://www.hsl.fi/"

datadir = "./data/HSL"

# See https://fi.wikipedia.org/wiki/Helsingin_seudun_liikenne#J%C3%A4senkunnat
cities = [
    "Helsinki", "Espoo", "Vantaa", "Kirkkonummi", "Kerava", "Kauniainen",
    "Sipoo", "Tuusula", "Siuntio"]

# Cities with some bus stops with HSL code
extracities_bus = [
    "Askola", "Järvenpää", "Mäntsälä", "Nurmijärvi", "Pornainen", "Porvoo", "Riihimäki",
    "Vihti"]  # Zone C extends to Vihti in Kattila

extracities_stations = ["Järvenpää"]

# Source: SVG icons from tyyliopas.hsl.fi (Piktogrammikirjasto)
modecolors = {
    "bus": "#007AC9",
    "tram": "#00985F",
    "light_rail": "#00B2A9",
    "train": "#8C4799",
    "subway": "#FF6319",
    "ferry": "#00B9E4",
    "aerialway": None,
    "monorail": None,
    "trolleybus": None
}

shapetols = {k: 30.0 for k in modecolors.keys()}
shapetols["train"] = 100.0
shapetols["ferry"] = 100.0

peakhours = [(7, 9), (15, 18)]
# HSL night services start at 23, but use midnight to avoid overlap with
# normal services.
nighthours = [(0, 5)]

# Assigned bus code prefixes for cities
prefix2city = {
    "": "Helsinki",
    "As": "Askola",
    "E": "Espoo",
    "H": "Helsinki",
    "Hy": "Hyvinkää",
    "Jä": "Järvenpää",
    "Ka": "Kauniainen",
    "Ke": "Kerava",
    "Ki": "Kirkkonummi",
    "La": "Lahti",
    "Mä": "Mäntsälä",
    "Nu": "Nurmijärvi",
    "Pn": "Pornainen",
    "Po": "Porvoo",
    "Ri": "Riihimäki",
    "Si": "Sipoo",
    "So": "Siuntio",
    "Tu": "Tuusula",
    "V": "Vantaa",
}

city2prefixes = defaultdict(list)
for p, c in prefix2city.items():
    city2prefixes[c].append(p)

# See https://fi.wikipedia.org/wiki/Luettelo_Suomen_kuntanumeroista
city2ref = {
    "Askola": "018",
    "Espoo": "049",
    "Helsinki": "091",
    "Hyvinkää": "106",
    "Iitti": "142",
    "Järvenpää": "186",
    "Kauniainen": "235",
    "Kerava": "245",
    "Kirkkonummi": "257",
    "Kouvola": "286",
    "Lahti": "398",
    "Mäntsälä": "505",
    "Nurmijärvi": "543",
    "Orimattila": "560",
    "Pornainen": "611",
    "Porvoo": "638",
    "Riihimäki": "694",
    "Sipoo": "753",
    "Siuntio": "755",
    "Tuusula": "858",
    "Vantaa": "092",
    "Vihti": "927",
}


zoneid2name = {
    "01": "A",
    "02": "B",
    "04": "C",
    "06": "D",  # Kirkkonummi, Siuntio
    "09": "D",  # Tuusula, Kerava, Sipoo
    "99": "no",
}


synonyms = [
    ('A. Petreliuksen', 'Albert Petreliuksen'),
    ('A.I.', 'A. I.'),
    ('Aleksant.', 'Aleksanterin'),
    ('Aurinkol.', 'Aurinkolahden'),
    ('Central Railway Station/East', 'Central Railway Station / East'),
    ('Central Railway Station/West', 'Central Railway Station / West'),
    ('Etel.', 'Eteläinen'),
    ('G. Paulig', 'Gustav Paulig'),
    ('Gyldenintie', 'Gyldénintie'),
    ('Hels.pit.', 'Helsingin pitäjän'),
    ('Hertton.terv.as.', 'Herttoniemen terveysasema'),
    ('Hoplaks', 'Hoplax'),
    ('Hospital', 'hospital'),
    ('Juhana Herttuan', 'Juhana-herttuan'),
    ('Järvenpää L.-autoas.', 'Järvenpään linja-autoasema'),
    ('Kallioimarteenkj', 'Kallioimarteenkuja'),
    ('Kannistonti', 'Kannistontie'),
    ('Kenttäpäällikönpuist', 'Kenttäpäällikönpuisto'),
    ('Korkea', 'Korkeakouluaukio'),
    ('Korkeakoulua', 'Korkeakouluaukio'),
    ('Kunnallisneuvoksent', 'Kunnallisneuvoksentie'),
    ('Laversinti', 'Laversintie'),
    ('Luonnontiet.museo', 'Luonnontieteellinen museo'),
    ('Länsiterm.', 'Länsiterminaali'),
    ('matkahuolto', 'Matkahuolto'),
    ('Mattgårdvägen', 'Mattgårdsvägen'),
    ('Martinl. terv. asema', 'Martinlaakson terveysasema'),
    ('Meilahden s.', 'Meilahden sairaala'),
    ('Metrovarikko', 'metrovarikko'),
    ('Munkkin.', 'Munkkiniemen'),
    # https://www.kielikello.fi/-/jatkasaarenlaituri-ja-kuninkaanhaanaukio-kaavanimien-oikeinkirjoituksesta
    ('Mäkipellon aukio', 'Mäkipellonaukio'),
    ('Olympiaterm.', 'Olympiaterminaali'),
    ('Oststranden', 'Otstranden'),
    ('Palokaivon aukio', 'Palokaivonaukio'),
    ('Pasila as.', 'Pasilan asema'),
    ('Pohj.', 'Pohjoinen'),
    ('Rautatieas', 'Rautatieasema'),
    ('Rautatieas.', 'Rautatieasema'),
    ('Saariselän palvelut.', 'Saariseläntien palvelutalo'),
    ('Saariselänt. palv.t.', 'Saariseläntien palvelutalo'),
    ('Sipoonl. levähd.al.', 'Sipoonlahden levähdysalue'),
    ('Siren', 'Sirén'),
    ('Station', 'station'),
    ('Sörn. (M)', 'Sörnäinen (M)'),
    # The apostrophe should be U+2019 according to
    # https://fi.wikipedia.org/wiki/Heittomerkki but U+0027 is easier to type
    # ('Tarkk´ampujankatu', 'Tarkk’ampujankatu'),
    ('Tarkk´ampujankatu', "Tarkk'ampujankatu"),
    ('Terv.', 'Terveys'),
    ('th', 'tienhaara'),
    ('tiehaara', 'tienhaara'),
    ('Tv', 'TV'),
    ('Viikinm.', 'Viikinmäen'),
    ('ammattik', 'ammattikoulu'),
    ('ammattik.', 'ammattikoulu'),
    ('as.', 'asema'),
    ('hautausm', 'hautausmaa'),
    ('huoltol', 'huoltolaituri'),
    ('koulukesk', 'koulukeskus'),
    ('koulukesk.', 'koulukeskus'),
    ('kesk.', 'keskus'),
    ('kesk.', 'keskusta'),
    ('kp.', 'kääntöpaikka'),
    ('la-as.', 'linja-autoasema'),
    ('linja-autoas.', 'linja-autoasema'),
    ('lab.', 'laboratorio'),
    ('leikkiniitt', 'leikkiniitty'),
    ('matkakesk', 'matkakeskus'),
    ('mk', 'matkakeskus'),
    ('op.', 'opisto'),
    ('opetusp.', 'opetuspiste'),
    ('ostosk.', 'ostoskeskus'),
    ('ostoskesk', 'ostoskeskus'),
    ('p', 'polku'),
    ('p.', 'puisto'),
    ('palv.talo', 'palvelutalo'),
    ('palveluk', 'palvelukeskus'),
    ('palstatie poh', 'palstatie pohj.'),
    ('poh', 'pohjoinen'),
    ('puist.', 'puistikko'),
    ('päälait', 'päälaituri'),
    ('ristey', 'risteys'),
    ('rist.', 'risteys'),
    ('siirt.puutarha', 'siirtolapuutarha'),
    ('t.', 'tie'),
    ('term.', 'terminaali'),
    ('terv.as.', 'terveysasema'),
    ('terv.asema', 'terveysasema'),
    ('terveysas.', 'terveysasema'),
    ('terveysas', 'terveysasema'),
    ('teollisuusk', 'teollisuuskatu'),
    ('Tietot', 'Tietotie'),
    ('Työväen akatemia', 'Työväen Akatemia'),
    ('uimah.', 'uimahalli'),
    ('urheiluk.', 'urheilukenttä'),
    ('urheiluk.', 'urheilukeskus'),
    ('vanhaink.', 'vanhainkoti'),
    ('vanhaink', 'vanhainkoti'),
    ('vari', 'varikko'),
    ('virastot.', 'virastotalo'),
    ('Östersund.', 'Östersundomin'),
]

# Sort to descending length of abbreviation
synonyms.sort(key=lambda x: (len(x[0]), x[1], x[0]), reverse=True)

synonyms_dict = dict(synonyms)


def get_overpass_area(clist):
    afmt = 'area[boundary=administrative][admin_level=8][name="{}"][ref={}];'
    area = "(" + ' '.join([afmt.format(c, city2ref[c]) for c in clist])
    area += ")->.hel;"
    return area


overpass_area = get_overpass_area(cities)
# Stops can also be in cities outside of HSL area
overpass_stopref_area = get_overpass_area(set(prefix2city.values()))


def longname2stops(longname):
    # First, replace hyphens in known stop names and split.
    # Then replace back to get a stop list from HSL longName.

    # Match list from gtfs stops.txt (data/digiroad/stops/HSL/stops.txt)
    # csvtool namedcol "stop_name" stops.txt | grep -o '.*-[[:upper:]]...' | sort -u  # noqa: E501
    known_hyphenated = [
        'Ala-Malm',
        'Ala-Souk',
        'Ala-Tikk',
        'Etelä-Kask',
        'Etelä-Viin',
        'Helsinki-Vant',
        'Itä-Hakk',
        'Kala-Matt',
        'Koivu-Mank',
        'Lill-Beng',
        'Meri-Rast',
        'Övre-Juss',
        'P-Huop',
        'Pohjois-Haag',
        'Pohjois-Viin',
        'S-Mark',
        'Stor-Kvis',
        'Stor-Rösi',
        'Taka-Niip',
        'Vanha-Mank',
        'Vanha-Sten',
        'Ylä-Mank',
        'Ylä-Souk',
        'Yli-Finn',
    ]
    # Other place names with hyphens
    extrapat = "|Pohjois-Nikinmä|Länsi-Pasila|Etelä-Leppäv"
    pat = '|'.join(known_hyphenated) + extrapat
    # Handle '(Inkoo-)blah-...'
    longname = re.sub(r"^\(([^-]*-)\)(.*)$", r"\1\2", longname)
    # Handle '...-blah(-Inkoo)'
    longname = re.sub(r"(.*)\((-.*)\)$", r"\1\2", longname)

    def subf(m):
        return m.group().replace('-', '☺')
    stops = re.sub(pat, subf, longname).split('-')
    stops = [s.replace('☺', '-').strip() for s in stops]
    return stops


def get_platform(ps):
    return ps.get("platformCode")


# def get_stopname(ps):
#    """Return stop name composed from 'name' and 'platformCode' fields."""
#    pname = ps.get("name", None)
#    pplat = ps.get("platformCode", None)
#    namecount = ps.get("namecount", None)
#    req_plat = ((namecount and namecount > 2) or not namecount
#      or ps.get('mode', 'bus') == 'train')
#    if req_plat and pname and pplat and pplat[0].isnumeric():
#        pn = "{}, laituri {}".format(pname, pplat)
#    else:
#        pn = pname
#    return pn


def normalize_helsinki_codes(dd, change_code=False):
    """Convert keys of the type 'dddd' in input dict to 'Hdddd'."""
    pattern = re.compile("^[0-9]{4,4}$")
    for r in list(dd.keys()):  # need a static copy of the keys
        if pattern.match(r):
            newref = 'H' + r
            if change_code and dd[r].get('code'):
                dd[r]['code'] = newref
            dd[newref] = dd.pop(r)
